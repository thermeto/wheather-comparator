# WheatherComparator Spring-Boot pet project
 
## Access WheatherComparator deployed in AWS Elastic Beanstalk environment
**(website tested and adjusted for horizontal display orientation (laptop etc.) so usability on phones is not guaranteed)**

http://weathercomparator-env.eba-qajiptfu.us-east-1.elasticbeanstalk.com/home

## Project idea and motivation

Idea behind the project and some details of its implementation described on the [HOME](http://weathercomparator-env.eba-qajiptfu.us-east-1.elasticbeanstalk.com/home) page in the **Objectives** section (bottom of the page). Proposals regarding the functionality extension or other improvements you may leave at the **Proposal** tab.

## Running WheatherComparator locally

WheatherComparator is a Spring Boot application built using Maven. You can build a jar file and run it from the command line (it should work just as well with Java 11 or newer):

1) Clone project via command line:
```
git clone https://github.com/thermeto/wheather-comparator.git
```
2) Choose IDE 
Eclipse or STS:
File -> Import -> Maven -> Existing Maven project
IntelliJ IDEA
In the main menu, choose File -> Open and select the wheather-comparator pom.xml. Click on the Open button.

3) Define DB connection in [application.properties](https://github.com/thermeto/wheather-comparator/blob/d679560dea8859bd53146113acc9b24b781dd9fd/src/main/resources/application.properties).

4) Table schema and initial table population you can find at [src/main/resources/weathercomparator_schema.sql](https://github.com/thermeto/wheather-comparator/blob/1f93cd4acc50761de89e581e3f6e6350274371a1/src/main/resources/weathercomparator_schema.sql) and [weathercomparator-data.sql](https://github.com/thermeto/wheather-comparator/blob/1f93cd4acc50761de89e581e3f6e6350274371a1/src/main/resources/weathercomparator-data.sql) respectively. _At this point DB is empty so charts and tables on the Home page will be blank._ 

4) Run the application main method by right clicking on it and choosing Run As -> Java Application.

You can then access WheatherComparator here: http://localhost:5000/

## Spring boot project structure details

* Project implemented in traditional MVC design pattern. 
* However, there is service - **BackgroundExecutor** - responsible for periodic weather (forecast and actual) samples aggregation and analysis on them that must run regardless the rest of the MVC part implementation. This service is isolated from the standard MVC components. MVC components consume only products of BackgroundExecutor stored in DB. 
* Sole point of direct interaction between BackgroundExecutor  and web frontend is correlation coefficients update and request to recalc all the generated weather control and summary points.
* Simplified UML diagram of the **BackgroundExecutor** service given below:

<img width="1042" alt="simple-uml" src="https://github.com/thermeto/wheather-comparator/blob/eb0ab6a30e62f1f239a1bbcbb89223cec8a55975/Simple.png">
