package com.pet.wheathercomparator.controller;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.constants.CityList;
import com.pet.wheathercomparator.constants.ForecastRange;
import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.model.chartconfig.ChartSessionConfig;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.service.HomeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;

import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;

import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@AutoConfigureMockMvc
@SpringBootTest
class HomeControllerTest {
    private static MockHttpServletRequest postConfigRequest;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private HomeService homeServiceMock;
    @MockBean
    private ChartSessionConfig chartSessionConfigsMock;
    @BeforeAll
    public static void setup() {
        postConfigRequest = new MockHttpServletRequest();
        postConfigRequest.addParameter("selectedProviders", new String[]{"sinoptik"});
        postConfigRequest.addParameter("selectedRange", ForecastRange.THREEHOUR.getDisplayName());
        postConfigRequest.addParameter("selectedCity", CityList.KYIV.getCity());
        postConfigRequest.addParameter("selectedAnalysisType", AnalysisType.GENERAL_CONDITION.getAnalysisType());
    }
    @Test
    @WithAnonymousUser
    void displayHomePage_shouldReturnMavWithTables_whenPlainRequest() throws Exception {
        when(homeServiceMock.buildChartDataTable()).thenReturn(new ArrayList<>());
        when(homeServiceMock.buildSummaryDataTable()).thenReturn(new ArrayList<>());
        when(homeServiceMock.getConditionCorrelationsFullList()).thenReturn(new ConditionMatrix());

        MvcResult mvcResult = mockMvc.perform(get("/")).andExpect(status().isOk()).andReturn();
        ModelAndView mav = mvcResult.getModelAndView();

        assertTrue(!mav.getModelMap().containsKey("hasErrors"));
        ModelAndViewAssert.assertViewName(mav, "home.html");
        verify(homeServiceMock).buildChartDataTable();
        verify(homeServiceMock).buildSummaryDataTable();
        verify(homeServiceMock).getConditionCorrelationsFullList();
    }

    @Test
    @WithMockUser
    void displayHomePage_shouldMapUser_whenLoggedIn() throws Exception {
        User userMock = mock(User.class);
        when(homeServiceMock.mapPersonToUser(anyString())).thenReturn(userMock);
        when(homeServiceMock.buildChartDataTable()).thenReturn(new ArrayList<>());
        when(homeServiceMock.buildSummaryDataTable()).thenReturn(new ArrayList<>());
        when(homeServiceMock.getConditionCorrelationsFullList()).thenReturn(new ConditionMatrix());

        MvcResult mvcResult = mockMvc.perform(get("/home?loggedIn=true")).andExpect(status().isOk()).andReturn();
        ModelAndView mav = mvcResult.getModelAndView();

        verify(homeServiceMock).mapPersonToUser(anyString());
        ModelAndViewAssert.assertViewName(mav, "home.html");
    }
    @Test
    void configureChart_shouldUpdateSessionConfig_whenPostValidConfigs() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/applyChartChanges").contentType(MediaType.APPLICATION_JSON)
                        .param("selectedProviders", postConfigRequest.getParameterValues("selectedProviders"))
                        .param("selectedRange", postConfigRequest.getParameterValues("selectedRange"))
                        .param("selectedCity", postConfigRequest.getParameterValues("selectedCity"))
                        .param("selectedAnalysisType", postConfigRequest.getParameterValues("selectedAnalysisType")))
                .andExpect(status().is3xxRedirection()).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();

        ModelAndViewAssert.assertViewName(mav, "redirect:/");
        verify(chartSessionConfigsMock).setSelectedProviders(anyList());
        verify(chartSessionConfigsMock).setSelectedRange(anyString());
        verify(chartSessionConfigsMock).setUnits(anyString());
        verify(chartSessionConfigsMock).setSelectedCity(anyString());
        verify(chartSessionConfigsMock).setSelectedAnalysisType(anyString());
    }
    @Test
    void saveProposal_Success() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/saveProposal").contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Vlad")
                        .param("email", "email@gmail.com")
                        .param("subject", "other")
                        .param("details", "details of the proposal")
                        .param("type", "other"))
                .andExpect(status().is3xxRedirection()).andReturn();

        ModelAndView mav = mvcResult.getModelAndView();

        ModelAndViewAssert.assertViewName(mav, "redirect:/");
    }
    @Test
    void saveProposal_shouldRedirectValidError_whenPostInvalidProposal() throws Exception {
        Mockito.doNothing().when(homeServiceMock).saveProposal(any(Proposal.class));

        var mvcResult = mockMvc.perform(post("/saveProposal").contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Vlad")
                        .param("email", "email@gmail.com")
                        .param("subject", "")
                        .param("details", "details of the proposal")
                        .param("type", "other"))
                .andExpect(status().is3xxRedirection()).andReturn();

        assertTrue(mvcResult.getFlashMap()!=null);
        assertTrue((Boolean) mvcResult.getFlashMap().get("hasErrors"));
    }
}