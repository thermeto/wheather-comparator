package com.pet.wheathercomparator.service;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.model.Person;
import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.model.chartconfig.ChartSessionConfig;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.model.datatable.DataTableDirector;
import com.pet.wheathercomparator.model.datatable.builder.ChartDataTableBuilder;
import com.pet.wheathercomparator.model.datatable.builder.StandardTableBuilder;
import com.pet.wheathercomparator.repository.ConditionMatrixRepository;
import com.pet.wheathercomparator.repository.PersonRepository;
import com.pet.wheathercomparator.repository.ProposalRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class HomeServiceTest {

    @Mock
    private ProposalRepository proposalRepositoryMock;
    @Mock
    private PersonRepository personRepositoryMock;
    @MockBean
    private ChartSessionConfig sessionConfigMock;
    @Mock
    private DataTableDirector dataTableDirectorMock;
    @Mock
    private ConditionMatrixRepository conditionMatrixRepositoryMock;

    @InjectMocks
    private HomeService homeService;
    @Test
    void buildChartDataTable_shouldBuildGeneralCondTable_whenAnalysisTypeGeneralCond() {
        when(sessionConfigMock.getSelectedCity()).thenReturn("");
        when(sessionConfigMock.getSelectedRange()).thenReturn("");
        when(sessionConfigMock.getSelectedProviders()).thenReturn(new ArrayList<>());
        when(sessionConfigMock.getSelectedAnalysisType()).thenReturn(AnalysisType.GENERAL_CONDITION.getAnalysisType());

        homeService.buildChartDataTable();

        verify(dataTableDirectorMock).prepareConditionChart(any(ChartDataTableBuilder.class), anyString(), anyList(), anyInt());
    }

    @Test
    void buildChartDataTable_shouldBuildTemperatureTable_whenAnalysisTypeTemperature() {
        when(sessionConfigMock.getSelectedCity()).thenReturn("");
        when(sessionConfigMock.getSelectedRange()).thenReturn("");
        when(sessionConfigMock.getSelectedProviders()).thenReturn(new ArrayList<>());
        when(sessionConfigMock.getSelectedAnalysisType()).thenReturn(AnalysisType.TEMPERATURE.getAnalysisType());

        homeService.buildChartDataTable();

        verify(dataTableDirectorMock).prepareTemperatureChart(any(ChartDataTableBuilder.class), anyString(), anyList(), anyInt());
    }

    @Test
    void buildSummaryDataTable() {
        when(sessionConfigMock.getSelectedCity()).thenReturn("");
        when(sessionConfigMock.getSelectedRange()).thenReturn("");
        when(sessionConfigMock.getSelectedProviders()).thenReturn(new ArrayList<>());
        when(sessionConfigMock.getSelectedAnalysisType()).thenReturn(AnalysisType.TEMPERATURE.getAnalysisType());

        homeService.buildSummaryDataTable();

        verify(dataTableDirectorMock).prepareSummaryTable(
                any(StandardTableBuilder.class), anyString(), anyList(), anyString(), anyInt(), anyString());
    }
    @Test
    @WithAnonymousUser
    void saveProposal_shouldSaveProposal_whenAnonymousUser() {
        Proposal proposal = new Proposal();

        homeService.saveProposal(proposal);

        verify(proposalRepositoryMock).save(proposal);
        verifyNoInteractions(personRepositoryMock);
    }

    @Test
    @WithMockUser
    void saveProposal_shouldFinishAndSaveProposal_whenAuthUser() {
        Proposal proposal = new Proposal();
        Person personMock = mock(Person.class);
        when(personRepositoryMock.findByEmail("user")).thenReturn(personMock);
        when(personMock.getName()).thenReturn("userName");

        homeService.saveProposal(proposal);

        assertTrue(proposal.getName().equals(personMock.getName()));
        verify(personRepositoryMock).findByEmail(anyString());
        verify(proposalRepositoryMock).save(proposal);
    }

    @Test
    void getConditionCorrelationsFullList() {
        when(conditionMatrixRepositoryMock.readCorrelationCoefficients()).thenReturn(new ArrayList<>());

        homeService.getConditionCorrelationsFullList();

        verify(conditionMatrixRepositoryMock).readCorrelationCoefficients();
    }

    @Test
    void mapPersonToUser() {
        Person person = new Person();
        person.setName("name");
        person.setCity("city");
        person.setFavoriteProvider("provider");
        person.setEmail("email");
        person.setId(0);
        when(personRepositoryMock.findByEmail("")).thenReturn(person);

        User user = homeService.mapPersonToUser("");

        verify(personRepositoryMock).findByEmail("");
        assertTrue(user.getName().equals(person.getName()));
        assertTrue(user.getCity().equals(person.getCity()));
        assertTrue(user.getEmail().equals(person.getEmail()));
    }
}