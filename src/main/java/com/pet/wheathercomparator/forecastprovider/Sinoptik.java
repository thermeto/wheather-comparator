package com.pet.wheathercomparator.forecastprovider;

import com.pet.wheathercomparator.constants.CityList;
import com.pet.wheathercomparator.constants.ForecastProviderList;
import com.pet.wheathercomparator.repository.SinoptikConditionNamesRepository;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Sinoptik extends ForecastProvider {
    private final String forecastProvider = ForecastProviderList.SINOPTIK.getName();

    SinoptikConditionNamesRepository conditionNamesRepository;
    private final List<Pair<String,String>> cities = new ArrayList<>(){
        {
            //first element - cityName, second element - urlAlias
            add(new Pair<String,String> (CityList.KYIV.getCity(), "київ"));
            add(new Pair<String,String> (CityList.LVIV.getCity(), "львів"));
            add(new Pair<String,String> (CityList.ZHYTOMYR.getCity(), "житомир"));
            add(new Pair<String,String> (CityList.KHARKIV.getCity(), "харків"));
            add(new Pair<String,String> (CityList.ODESA.getCity(), "одеса"));
        }
    };
    private List<String> monitoredCityNames;
    private List<String> monitoredCityUrlAliases;
    @Autowired
    public Sinoptik(SinoptikConditionNamesRepository conditionNamesRepository){
        super();
        this.conditionNamesRepository = conditionNamesRepository;
        monitoredCityNames = new ArrayList<>();
        monitoredCityUrlAliases = new ArrayList<>();
        for(Pair<String,String> pair : cities){
            monitoredCityNames.add(pair.getValue0());
            monitoredCityUrlAliases.add(pair.getValue1());
        }
        this.providerConditionNames = conditionNamesRepository.readConditionNames();
    }
    @Override
    public List<String> getMonitoredCityNames(){
        return monitoredCityNames;
    }
    @Override
    public List<String> getMonitoredCityUrlAliases(){
        return monitoredCityUrlAliases;
    }
    @Override
    public String getForecastProviderName() {
        return forecastProvider;
    }
    @Override
    public List<String> getProviderConditionNames() {
        return this.providerConditionNames;
    }
}
