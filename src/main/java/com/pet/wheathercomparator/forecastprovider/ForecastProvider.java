package com.pet.wheathercomparator.forecastprovider;

import com.pet.wheathercomparator.model.correlationmatrix.ConditionDefinition;
import com.pet.wheathercomparator.repository.ConditionMatrixRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Component
public abstract class ForecastProvider {
    private static ConditionMatrixRepository conditionMatrixRepository;
    public abstract String getForecastProviderName();
    public abstract List<String> getMonitoredCityNames();
    public abstract List<String> getMonitoredCityUrlAliases();
    protected List<String> providerConditionNames;

    private static List<List<Double>> correlationMatrix;
    private List<String> conditionConventionalNames;
    @Autowired
    public final void setConditionMatrixRepository(ConditionMatrixRepository conditionMatrixRepository){
        this.conditionMatrixRepository = conditionMatrixRepository;
        this.conditionConventionalNames = conditionMatrixRepository.readConditionConventionalNames();
        updateLocalCorrelationMatrix();
    }
    public List<List<Double>> getCorrelationMatrix(){
        return this.correlationMatrix;
    }
    public static void updateLocalCorrelationMatrix(){
        List<List<Double>> matrix = new ArrayList<>();
        List<ConditionDefinition> coeffs = conditionMatrixRepository.readCorrelationCoefficients();
        for(ConditionDefinition row : coeffs){
            matrix.add(row.getCoefficientsList());
        }
        correlationMatrix = matrix;
    }
    public List<String> getConventionalConditionNames(){
        return this.conditionConventionalNames;
    }

    //Concrete forecast provider must have its own names for conditions, coefficients remain the same
    public abstract List<String> getProviderConditionNames();
    public String getConventionalConditionName(String providerConditionName){
        List<String> conventionalNames = this.getConventionalConditionNames();
        List<String> providerConditionNames = getProviderConditionNames();
        for (int i=0; i<conventionalNames.size(); i++) {
            if(providerConditionName.toLowerCase().contains(providerConditionNames.get(i).toLowerCase()))
                return conventionalNames.get(i);
        }
        return providerConditionName + ": name not found";
    }

}
