package com.pet.wheathercomparator.forecastprovider;

import com.pet.wheathercomparator.constants.CityList;
import com.pet.wheathercomparator.constants.ForecastProviderList;
import com.pet.wheathercomparator.repository.GismeteoConditionNamesRepository;
import com.pet.wheathercomparator.repository.SinoptikConditionNamesRepository;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Gismeteo extends ForecastProvider{
    GismeteoConditionNamesRepository conditionNamesRepository;
    private final String forecastProvider = ForecastProviderList.GISMETEO.getName();
    private List<String> monitoredCityNames;
    private List<String> monitoredCityUrlAliases;
    private final List<Pair<String,String>> cities = new ArrayList<>(){
        {
            //first element - cityName, second element - urlAlias
            add(new Pair<String,String> (CityList.KYIV.getCity(), "kyiv-4944"));
            add(new Pair<String,String> (CityList.LVIV.getCity(), "lviv-4949"));
            add(new Pair<String,String> (CityList.ZHYTOMYR.getCity(), "zhytomyr-4943"));
            add(new Pair<String,String> (CityList.KHARKIV.getCity(), "kharkiv-5053"));
            add(new Pair<String,String> (CityList.ODESA.getCity(), "odessa-4982"));
        }
    };
    @Autowired
    public Gismeteo(GismeteoConditionNamesRepository conditionNamesRepository){
        super();
        this.conditionNamesRepository = conditionNamesRepository;
        monitoredCityNames = new ArrayList<>();
        monitoredCityUrlAliases = new ArrayList<>();
        for(Pair<String,String> pair : cities){
            monitoredCityNames.add(pair.getValue0());
            monitoredCityUrlAliases.add(pair.getValue1());
        }
        this.providerConditionNames = conditionNamesRepository.readConditionNames();
    }
    @Override
    public String getForecastProviderName() {
        return forecastProvider;
    }
    @Override
    public List<String> getMonitoredCityNames() {
        return monitoredCityNames;
    }
    @Override
    public List<String> getMonitoredCityUrlAliases() {
        return monitoredCityUrlAliases;
    }
    @Override
    public List<String> getProviderConditionNames() {
        return this.providerConditionNames;
    }
}
