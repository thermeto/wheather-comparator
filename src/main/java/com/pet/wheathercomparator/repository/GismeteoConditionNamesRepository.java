package com.pet.wheathercomparator.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
@Repository
public class GismeteoConditionNamesRepository {
    @PersistenceContext
    EntityManager entityManager;

    public List<String> readConditionNames(){

        Query query = entityManager.createNativeQuery("SELECT c.condition_name FROM gismeteo_condition_names c ORDER BY c.severity_level ASC");

        return query.getResultList();
    }
}
