package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {

    Person findByEmail(String email);
    Person findById(int id);
}
