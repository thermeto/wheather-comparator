package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ControlPoint;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ControlPointRepository extends CrudRepository<ControlPoint, Integer>{
    @Query(value = "SELECT MAX(c.dateTime) FROM ControlPoint c WHERE c.city =:city AND c.provider = :provider")
    public Timestamp findLastSavedDateTime(String city, String provider);
    @Query(nativeQuery=true, value = "SELECT * FROM (SELECT * FROM control_points c WHERE c.city = :city " +
            "AND c.provider = :provider " +
            "AND c.forecast_range = :range " +
            "ORDER BY date_time DESC " +
            "LIMIT :noOfRecords) t " +
            "ORDER BY date_time ASC")
    public List<ControlPoint> findLastControlPoints(String city, String provider, int range, int noOfRecords);

    @Query(nativeQuery=true, value = "SELECT CONCAT('Actual: ', actual_condition, '/ Forecast: ', forecast_condition) " +
            "FROM (SELECT * FROM control_points c WHERE c.city = :city " +
            "AND c.provider = :provider " +
            "AND c.forecast_range = :range " +
            "ORDER BY date_time DESC " +
            "LIMIT :noOfRecords) t " +
            "ORDER BY date_time ASC")
    public List<String> findLastConditionNames(String city, String provider, int range, int noOfRecords);
    @Query(nativeQuery=true, value = "SELECT date_time FROM (SELECT * FROM control_points c WHERE c.city =:city " +
            "AND c.provider = :provider " +
            "AND c.forecast_range = :range " +
            "ORDER BY date_time DESC " +
            "LIMIT :noOfRecords) t " +
            "ORDER BY date_time ASC")
    public List<Timestamp> findLastRecordedDates(String city, String provider, int range, int noOfRecords);
    @Query(nativeQuery=true, value = "SELECT * FROM control_points c WHERE c.city =:city " +
            "AND c.provider = :provider " +
            "AND c.forecast_range = :range " +
            "AND c.date_time > :from")
    public List<ControlPoint> findControlPointsFromDate(Timestamp from, String city, String provider, int range);
    @Query(nativeQuery=true, value = "SELECT DISTINCT c.forecast_range " +
            "FROM control_points c WHERE c.city =:city " +
            "AND c.provider = :provider " +
            "AND c.date_time > :from " +
            "ORDER BY c.forecast_range ASC")
    public List<Integer> findDistinctRangesFromDate(String city, String provider, Timestamp from);

}

