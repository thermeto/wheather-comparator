package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ControlPoint;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Repository
public class ControlPointBufferRepository{

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void saveToBufferTable(ControlPoint cp){
        Query query = entityManager.createNativeQuery
                ("INSERT INTO control_points_buffer (control_id, date_time, forecast_range, provider, " +
                        "city, condition_corr, delta_t, actual_condition, forecast_condition) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        query.setParameter(1, cp.getControlPointId());
        query.setParameter(2, cp.getDateTime());
        query.setParameter(3, cp.getForecastRange());
        query.setParameter(4, cp.getProvider());
        query.setParameter(5, cp.getCity());
        query.setParameter(6, cp.getConditionCorrelation());
        query.setParameter(7, cp.getDeltaT());
        query.setParameter(8, cp.getActualCondition());
        query.setParameter(9, cp.getForecastCondition());
        query.executeUpdate();
    }

    @Transactional
    public void prepareBufferTable(){
        createBufferTable();
        deleteFromBufferTable();
    }
    @Transactional
    public void replaceOriginalTableWithBuffer(){
        renameOriginalTableToReserve();
        renameBufferTableToOriginal();
        dropReserveTable();
    }

    @Transactional
    private void renameOriginalTableToReserve(){

        Query query = entityManager.createNativeQuery
                ("RENAME TABLE control_points TO control_points_reserve");

        query.executeUpdate();
    }

    @Transactional
    private void renameBufferTableToOriginal(){

        Query query = entityManager.createNativeQuery
                ("RENAME TABLE control_points_buffer TO control_points");

        query.executeUpdate();
    }

    @Transactional
    private void dropReserveTable(){

        Query query = entityManager.createNativeQuery
                ("DROP TABLE control_points_reserve");

        query.executeUpdate();
    }

    @Transactional
    private void createBufferTable(){

        Query query = entityManager.createNativeQuery
                ("CREATE TABLE IF NOT EXISTS control_points_buffer (" +
                        "control_id INT, " +
                        "date_time TIMESTAMP DEFAULT NULL," +
                        " forecast_range INT," +
                        " provider VARCHAR(40) DEFAULT NULL," +
                        " city VARCHAR(40) DEFAULT NULL," +
                        " condition_corr DECIMAL(6,3) DEFAULT 0," +
                        " delta_t DECIMAL(3,1) DEFAULT 1," +
                        " actual_condition VARCHAR(40) DEFAULT NULL," +
                        " forecast_condition VARCHAR(40) DEFAULT NULL," +
                        " FOREIGN KEY (`control_id`)" +
                        "    REFERENCES forecast_weather_samples(`sample_id`));");

        query.executeUpdate();
    }

    @Transactional
    private void deleteFromBufferTable(){

        Query query = entityManager.createNativeQuery
                ("DELETE FROM control_points_buffer ");

        query.executeUpdate();
    }
}
