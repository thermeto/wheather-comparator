package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ActualWeatherSample;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
@Repository
public interface ActualWeatherRepository extends CrudRepository<ActualWeatherSample, Integer> {

    @Query(value = "SELECT s FROM ActualWeatherSample s WHERE s.dateTime >= :from AND s.city = :city AND s.provider = :source")
    public List<ActualWeatherSample> findSamplesRecordedAfter(Timestamp from, String city, String source);
}
