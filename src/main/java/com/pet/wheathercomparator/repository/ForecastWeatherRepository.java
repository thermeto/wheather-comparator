package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ForecastWeatherSample;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ForecastWeatherRepository extends CrudRepository<ForecastWeatherSample, Integer> {
    @Query(value = "SELECT s FROM ForecastWeatherSample s " +
            "WHERE s.forecastDateTime >= :from " +
            "AND s.city = :city " +
            "AND s.provider = :source")
    public List<ForecastWeatherSample> findSamplesRecordedAfter(Timestamp from, String city, String source);

}
