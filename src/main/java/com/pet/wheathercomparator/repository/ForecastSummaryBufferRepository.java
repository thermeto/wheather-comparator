package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ForecastSummary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Repository
public class ForecastSummaryBufferRepository {
    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void saveToBufferTable(ForecastSummary fs){
        Query query = entityManager.createNativeQuery
                ("INSERT INTO forecast_summary_buffer (date, city, provider, " +
                        "forecast_range, on_interval_of_days, condition_correlation_rsd, temperature_rsd) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)");
        query.setParameter(1, fs.getDate());
        query.setParameter(2, fs.getCity());
        query.setParameter(3, fs.getProvider());
        query.setParameter(4, fs.getForecastRange());
        query.setParameter(5, fs.getOnIntervalOfDays());
        query.setParameter(6, fs.getConditionCorrelationRSD());
        query.setParameter(7, fs.getTemperatureRSD());
        query.executeUpdate();
    }

    @Transactional
    public void prepareBufferTable(){
        createBufferTable();
        deleteFromBufferTable();
    }
    @Transactional
    public void replaceOriginalTableWithBuffer(){
        renameOriginalTableToReserve();
        renameBufferTableToOriginal();
        dropReserveTable();
    }

    @Transactional
    private void renameOriginalTableToReserve(){

        Query query = entityManager.createNativeQuery
                ("RENAME TABLE forecast_summary TO forecast_summary_reserve");

        query.executeUpdate();
    }

    @Transactional
    private void renameBufferTableToOriginal(){
        Query query = entityManager.createNativeQuery
                ("RENAME TABLE forecast_summary_buffer TO forecast_summary");

        query.executeUpdate();
    }

    @Transactional
    private void dropReserveTable(){

        Query query = entityManager.createNativeQuery
                ("DROP TABLE forecast_summary_reserve");

        query.executeUpdate();
    }

    @Transactional
    private void createBufferTable(){

        Query query = entityManager.createNativeQuery
                ("CREATE TABLE IF NOT EXISTS forecast_summary_buffer (" +
                        "  id INT AUTO_INCREMENT PRIMARY KEY," +
                        "  date TIMESTAMP NOT NULL," +
                        "  city VARCHAR(40) NOT NULL," +
                        "  provider VARCHAR(40) NOT NULL," +
                        "  forecast_range INT DEFAULT 1," +
                        "  on_interval_of_days INT DEFAULT 1," +
                        "  condition_correlation_rsd DECIMAL(4,1) DEFAULT 0," +
                        "  temperature_rsd DECIMAL(4,1) DEFAULT 0" +
                        ");");

        query.executeUpdate();
    }

    @Transactional
    private void deleteFromBufferTable(){

        Query query = entityManager.createNativeQuery
                ("DELETE FROM forecast_summary_buffer ");

        query.executeUpdate();
    }

}
