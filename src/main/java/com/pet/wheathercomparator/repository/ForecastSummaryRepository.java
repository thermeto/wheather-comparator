package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.weather.ForecastSummary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ForecastSummaryRepository extends CrudRepository<ForecastSummary, Integer> {
    @Query(nativeQuery=true, value = "SELECT s.condition_correlation_rsd FROM forecast_summary s " +
            "WHERE s.city =:city " +
            "AND s.provider = :provider " +
            "AND s.forecast_range = :range " +
            "AND s.date BETWEEN DATE_SUB(NOW(), INTERVAL 21 HOUR) AND DATE_SUB(NOW(), INTERVAL -3 HOUR) " + //DB timezone diff
            "AND s.on_interval_of_days IN(:onIntervals) " +
            "ORDER BY s.on_interval_of_days ASC")
    public List<Double> findLastConditionCorrelationRSD(String city, String provider,
                                                        int range, List<Integer> onIntervals);

    @Query(nativeQuery=true, value = "SELECT s.temperature_rsd FROM forecast_summary s " +
            "WHERE s.city =:city " +
            "AND s.provider = :provider " +
            "AND s.forecast_range = :range " +
            "AND s.date BETWEEN DATE_SUB(NOW(), INTERVAL 21 HOUR) AND DATE_SUB(NOW(), INTERVAL -3 HOUR) " + //DB timezone diff
            "AND s.on_interval_of_days IN(:onIntervals) " +
            "ORDER BY s.on_interval_of_days ASC")
    public List<Double> findLastTemperatureRSD(String city, String provider,
                                                        int range, List<Integer> onIntervals);
}
