package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.correlationmatrix.ConditionDefinition;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConditionMatrixRepository extends CrudRepository<ConditionDefinition, Integer> {

    @Query(nativeQuery=true, value ="SELECT * FROM condition_matrix m ORDER BY severity_level ASC")
    public List<ConditionDefinition> readCorrelationCoefficients();

    @Query(nativeQuery=true, value = "SELECT m.condition_name FROM condition_matrix m ORDER BY m.severity_level ASC")
    public List<String> readConditionConventionalNames();

}
