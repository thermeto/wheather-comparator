package com.pet.wheathercomparator.repository;

import com.pet.wheathercomparator.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role getByRoleName(String roleName);
}
