package com.pet.wheathercomparator.constants;

import com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.gismeteo.GismeteoScraper;

public enum AnalysisType {
    GENERAL_CONDITION("General condition", "%"),
    TEMPERATURE("Temperature", "°C");
    //PRECIPITATION("Precipitation", "mm");
    private String analysisType;
    private String units;
    AnalysisType(String analysisType, String units){
        this.analysisType = analysisType;
        this.units = units;
    }

    public String getAnalysisType(){
        return this.analysisType;
    }
    public String getUnits(){return this.units;}

    public static String getUnitsByAnalysisType (String analysisType){
        String units = "°C";
        for(AnalysisType type : AnalysisType.values()){
            if(type.getAnalysisType().equals(analysisType)) {
                units = type.getUnits();
                break;
            }
        }
        return units;
    }
}
