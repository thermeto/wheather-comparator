package com.pet.wheathercomparator.constants;

public enum CityList {
    KYIV("Kyiv"),
    ODESA("Odesa"),
    LVIV("Lviv"),
    KHARKIV("Kharkiv"),
    ZHYTOMYR("Zhytomyr"),
    DNIPRO("Dnipro"),
    FASTIV("Fastiv");

    private String city;

    CityList(String city){
        this.city = city;
    }

    public String getCity(){
        return city;
    }
}
