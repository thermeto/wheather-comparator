package com.pet.wheathercomparator.constants;

public enum SummaryTableInterval {
    PREV_DAY(1, "Previous day"),
    LAST_3_DAYS(3, "Last 3 days"),
    LAST_WEEK(7, "Last week"),
    LAST_2_WEEKS(14,"Last 2 weeks"),
    LAST_MONTH(30, "Last month");

    String intervalHeader;
    int interval;

    SummaryTableInterval(int interval, String intervalHeader){
        this.interval = interval;
        this.intervalHeader = intervalHeader;
    }

    public String getIntervalHeader(){
        return this.intervalHeader;
    }

    public int getInterval(){
        return this.interval;
    }

}
