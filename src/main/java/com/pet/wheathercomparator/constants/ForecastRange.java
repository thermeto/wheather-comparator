package com.pet.wheathercomparator.constants;

public enum ForecastRange {
    THREEHOUR(3, "3-hour"),
    SIXHOUR(6, "6-hour"),
    TWELVEHOUR(12, "12-hour"),
    EIGHTEENHOUR(18, "18-hour"),
    ONEDAY(24, "1-day"),
    TWODAY(48, "2-day"),
    THREEDAY(72, "3-day"),
    FOURDAY(96, "4-day"),
    FIVEDAY(120, "5-day"),
    SIXDAY(144, "6-day"),
    WEEK(168, "Week"),
    TENDAY(240, "10-day");
    private int range;
    private String displayName;
    ForecastRange(int range, String displayName) {
        this.range = range;
        this.displayName = displayName;
    }
    public int getRange(){

        return range;
    }

    public String getDisplayName(){
        return displayName;
    }
    public static int getRangeByDisplayName(String displayName){
        int range = 3;
        for(ForecastRange value : ForecastRange.values()){
            if(value.displayName.equals(displayName)) range = value.range;
        }
        return range;
    }

}
