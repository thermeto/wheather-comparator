package com.pet.wheathercomparator.constants;

public enum ForecastProviderList {
    SINOPTIK("sinoptik", "https://ua.sinoptik.ua/погода-"),
    GISMETEO("gismeteo", "https://www.gismeteo.ua/ua/weather-");

    private String name;
    private String baseUrl;
    ForecastProviderList(String name, String baseUrl){
        this.name = name;
        this.baseUrl = baseUrl;
    }

    public String getName(){
        return name;
    }

    public String getBaseUrl(){
        return baseUrl;
    }
}
