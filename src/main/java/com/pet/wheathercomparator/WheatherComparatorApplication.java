package com.pet.wheathercomparator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories("com.pet.wheathercomparator.repository")
@EntityScan("com.pet.wheathercomparator")
public class WheatherComparatorApplication {
	public static void main(String[] args) {

		SpringApplication.run(WheatherComparatorApplication.class, args);
	}
}
