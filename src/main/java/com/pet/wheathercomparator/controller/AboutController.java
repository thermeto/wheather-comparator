package com.pet.wheathercomparator.controller;

import com.pet.wheathercomparator.model.Proposal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/about")
public class AboutController {

    @RequestMapping(value="")
    public ModelAndView displayAbout(){
        ModelAndView modelAndView = new ModelAndView("about.html");
        modelAndView.addObject("proposal", new Proposal());
        return modelAndView;
    }
}
