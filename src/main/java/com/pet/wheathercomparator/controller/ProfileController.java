package com.pet.wheathercomparator.controller;

import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class ProfileController {
    @Autowired
    private PersonService service;

    @RequestMapping(value="/profile")
    public ModelAndView displayProfile (HttpSession session){
        ModelAndView modelAndView = new ModelAndView("profile.html");
        User user = (User) session.getAttribute("loggedInUser");

        modelAndView.addObject("proposal", new Proposal());
        modelAndView.addObject("user", user);

        return modelAndView;
    }

    @RequestMapping(value ="/updateProfile",method = { RequestMethod.POST} )
    public String updateProfile(@Valid @ModelAttribute("user") User user, Errors errors, HttpSession session) {
        if(errors.hasErrors()){
            return "profile.html";
        }
        User loggedInUser = (User) session.getAttribute("loggedInUser");
        user.setId(loggedInUser.getId());

        service.updatePerson(user);
        session.setAttribute("loggedInUser", user);
        return "redirect:/profile";
    }
}
