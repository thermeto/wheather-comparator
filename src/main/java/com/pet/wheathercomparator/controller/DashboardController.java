package com.pet.wheathercomparator.controller;

import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.Role;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.service.DashboardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@Slf4j
@RequestMapping(value = "/dashboard")
public class DashboardController {
    @Autowired
    private DashboardService service;
    @RequestMapping(value="")
    public ModelAndView displayDashboard(@RequestParam(required = false) boolean formSubmitted,
                                         @RequestParam(required = false) boolean oldRecalcInProcess){
        ModelAndView modelAndView = new ModelAndView("dashboard.html");

        modelAndView.addObject("proposal", new Proposal());
        modelAndView.addObject("formSubmitted", formSubmitted);
        modelAndView.addObject("oldRecalcInProcess", oldRecalcInProcess);
        return modelAndView;
    }

    @RequestMapping(value= "/correlation_matrix")
    public ModelAndView displayCorrelationMatrix(@RequestParam(required = false) boolean changesAbsent,
                                                @RequestParam(required=false) boolean isDemo){
        ModelAndView modelAndView = new ModelAndView("correlation_matrix.html");

        ConditionMatrix matrix = service.getConditionCorrelationsFullList();

        modelAndView.addObject("proposal", new Proposal());
        modelAndView.addObject("changesAbsent", changesAbsent);
        modelAndView.addObject("isDemo", isDemo);
        modelAndView.addObject("matrix", matrix);

        return modelAndView;
    }
    @PostMapping(value = "/correlation_matrix/saveNewCoefficients")
    public ModelAndView saveNewCoefficients(@Valid @ModelAttribute("matrix") ConditionMatrix matrix, BindingResult result){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER")))
            return new ModelAndView("redirect:/dashboard/correlation_matrix?isDemo=true");

        service.applyNewCoefficientsToMatrixForm(matrix);

        if(matrix.equals(service.getConditionCorrelationsFullList()))
            return new ModelAndView("redirect:/dashboard/correlation_matrix?changesAbsent=true");

        if(!service.inputIsValid(matrix)) {
            ModelAndView modelAndView = new ModelAndView("correlation_matrix.html");
            modelAndView.addObject("matrix", matrix);
            result.rejectValue("matrix","coefficient.range", "Coefficients must not exceed [-1..1] range");
            return modelAndView;
        }
        boolean recalcLaunched = service.tryLaunchForecastAnalysisWithNewCoeffs(matrix);

        if(recalcLaunched) return new ModelAndView("redirect:/dashboard?formSubmitted=true");
        else return new ModelAndView("redirect:/dashboard?oldRecalcInProcess=true");
    }
}
