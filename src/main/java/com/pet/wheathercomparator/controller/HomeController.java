package com.pet.wheathercomparator.controller;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.model.chartconfig.ChartSessionConfig;
import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.repository.PersonRepository;
import com.pet.wheathercomparator.service.HomeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
@Controller
@Slf4j
public class HomeController {
    @Autowired
    private HomeService service;
    @Autowired
    private ChartSessionConfig chartSessionConfigs;

    @Autowired
    private PersonRepository personRepository;

    @RequestMapping(value={"", "/", "home"})
    public ModelAndView displayHomePage(@RequestParam(required = false) boolean loggedIn,
                                        Model model, HttpSession session) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(loggedIn & !(authentication instanceof AnonymousAuthenticationToken)){
            String currentUserEmail = authentication.getName();
            User user = service.mapPersonToUser(currentUserEmail);
            session.setAttribute("loggedInUser", user);
        }

        List<List<Object>> chartDataTable = service.buildChartDataTable();
        List<List<Object>> summaryTable = service.buildSummaryDataTable();
        ConditionMatrix matrix = service.getConditionCorrelationsFullList();

        ModelAndView modelAndView = new ModelAndView("home.html");
        modelAndView.addObject("chartConfigs", this.chartSessionConfigs);
        modelAndView.addObject("chartDataTable", chartDataTable);
        modelAndView.addObject("summaryTable", summaryTable);
        modelAndView.addObject("proposal", new Proposal());
        modelAndView.addObject("matrix", matrix);

        //for validations inside modal propose
        if(model.asMap().containsKey("hasErrors")) {
            var hasErrors = model.asMap().get("hasErrors");
            var errors = model.asMap().get("errors");
            var failedProposal = model.asMap().get("failedProposal");

            modelAndView.addObject("hasErrors", hasErrors);
            modelAndView.addObject("errors", errors);
            modelAndView.addObject("failedProposal", failedProposal);
        }
        return modelAndView;
    }
    @PostMapping(value = "/applyChartChanges")
    public String configureChart(@ModelAttribute("chartConfigs") ChartSessionConfig chartConfigs)
        {
        this.chartSessionConfigs.setSelectedCity(chartConfigs.getSelectedCity());
        this.chartSessionConfigs.setSelectedProviders(chartConfigs.getSelectedProviders());
        this.chartSessionConfigs.setUnits(AnalysisType.getUnitsByAnalysisType(chartConfigs.getSelectedAnalysisType()));
        this.chartSessionConfigs.setSelectedRange(chartConfigs.getSelectedRange());
        this.chartSessionConfigs.setSelectedAnalysisType(chartConfigs.getSelectedAnalysisType());
        return "redirect:/";
    }
    @PostMapping(value = "/saveProposal")
    public String saveProposal(@Valid @ModelAttribute("proposal") Proposal proposal, BindingResult result, RedirectAttributes attr){
        if(result.hasErrors()){
            attr.addFlashAttribute("hasErrors", true);
            attr.addFlashAttribute("errors", result.getFieldErrors());
            attr.addFlashAttribute("failedProposal", proposal);
            return "redirect:/";
        }
        service.saveProposal(proposal);
        return "redirect:/";
    }
}
