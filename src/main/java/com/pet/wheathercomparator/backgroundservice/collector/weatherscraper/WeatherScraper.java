package com.pet.wheathercomparator.backgroundservice.collector.weatherscraper;

public interface WeatherScraper {
    public String currentGeneralCondition(String city);
    public int currentTemperature(String city);
    public String forecastGeneralCondition(String city, long plusHours);
    public int forecastTemperature(String city, long plusHours);
    public String getProviderName();
}
