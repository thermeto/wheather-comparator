package com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.gismeteo;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GismeteoParser {

    public List<Integer> parseWeatherTimeSamples(Document page) {
        List<String> rawTimelineListForCurrDay = page.getElementsByClass("widget-row widget-row-time")
                .get(0)
                .getElementsByTag("span")
                .eachText();

        List<Integer> timeSamples = new ArrayList<>();

        for(String rawSample : rawTimelineListForCurrDay){
            String trimmedSample = rawSample.substring(0,rawSample.length()-2);//last 2 chars are minutes
            timeSamples.add(Integer.valueOf(trimmedSample));
        }
        return timeSamples;
    }

    public List<Integer> parseTemperaturesSamples(Document page){
        List<String> temperatureSamplesString = page.getElementsByClass("widget-row-chart widget-row-chart-temperature")
                .get(0)
                .getElementsByClass("unit unit_temperature_c")
                .eachText();

        List<Integer> temperatureSamplesInt = temperatureSamplesString.stream()
                .mapToInt(x->parseTemperature(x))
                .boxed()
                .collect(Collectors.toList());

        return temperatureSamplesInt;
    }
    public String parseCurrentGeneralCondition(Document page){
        String condition = page.getElementsByClass("now-desc")
                .eachText().get(0);
        return condition;
    }
    public int parseCurrentTemperature(Document page){
        String rawCurrentTemp = page.getElementsByClass("unit unit_temperature_c")
                .eachText().get(0);

        return parseTemperature(rawCurrentTemp);
    }
    public int parseTemperature(String rawString){
        String intermediate = rawString.replace("+","").split(",")[0];
        return Integer.valueOf(intermediate);
    }

    public List<String> parseGeneralConditions(Document page){
        List<String> conditions = page.getElementsByClass("widget-row widget-row-icon")
                .get(0).getElementsByClass("weather-icon tooltip")
                .eachAttr("data-text");
        return conditions;
    }
}
