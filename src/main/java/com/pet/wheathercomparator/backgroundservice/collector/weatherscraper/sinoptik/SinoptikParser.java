package com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.sinoptik;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

public class SinoptikParser {
    public List<Integer> parseWeatherTimeSamples(Document page){
        List<String> rawTimelineListForCurrDay = page.getElementsByClass("gray time")
                .get(0)
                .getElementsByTag("td")
                .eachText();

        List<String> stringTimelineListForCurrDay = rawTimelineListForCurrDay
                .stream().map(x->x.substring(0, x.indexOf(" ")))
                .collect(Collectors.toList());

        List<Integer> intTimelineListForCurrDay = stringTimelineListForCurrDay.stream()
                .mapToInt(x->Integer.valueOf(x))
                .boxed()
                .collect(Collectors.toList());
        return intTimelineListForCurrDay;
    }

    public int parseTemperature(String rawString){
        String intermediate = rawString.replace("+","").replace("°","")
                .replace("C","");
        return Integer.valueOf(intermediate);
    }

    public List<Integer> parseTemperaturesSamples(Document page){
        //first 7 blocks for other days
        List<String> temperatureSamplesString = page.getElementsByClass("temperature").get(7)
                .getElementsByTag("td")
                .eachText();

        List<Integer> temperatureSamplesInt = temperatureSamplesString.stream()
                .mapToInt(x->parseTemperature(x))
                .boxed()
                .collect(Collectors.toList());

        return temperatureSamplesInt;
    }

    public String parseCurrentGeneralCondition(Document page){
        String condition = page.getElementsByClass("imgBlock").get(0)
                .getElementsByClass("img").get(0)
                .getElementsByAttribute("alt").
                eachAttr("alt").get(0);
        return condition;
    }
    public List<String> parseGeneralConditions(Document page){
        List<String> conditions = page.getElementsByClass("img weatherIcoS").
                get(0).getElementsByTag("div").
                eachAttr("title");
        return conditions;
    }
    public int parseCurrentTemperature(Document page){
        Element rawCurrentTempBlock = page.getElementsByClass("today-temp").get(0);
        Elements rawCurrentTempElement = rawCurrentTempBlock.getElementsByTag("p");
        String rawCurrentTemp = rawCurrentTempElement.eachText().get(0);
        return parseTemperature(rawCurrentTemp);
    }
}
