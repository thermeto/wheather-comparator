package com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.sinoptik;

import com.pet.wheathercomparator.constants.ForecastProviderList;
import com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.WeatherScraper;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class SinoptikScraper implements WeatherScraper {
    private final String baseUrl= ForecastProviderList.SINOPTIK.getBaseUrl();

    private SinoptikParser parser = new SinoptikParser();
    private Document getPageFor(String cityUrlAlias, long hoursFromNow) {
        Document page = null;

        LocalDate forecastDate = LocalDateTime.now().plusHours(hoursFromNow).toLocalDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        String forecastString = forecastDate.format(formatter);
        String concreteUrl = this.baseUrl+cityUrlAlias+"/"+forecastString;

        try{
            page = Jsoup.connect(concreteUrl).get();
        }
        catch (IOException e){
            log.warn("Was not able to reach page: " + concreteUrl);
        }
        return page;
    }
    @Override
    public String currentGeneralCondition(String cityUrlAlias) {
        Document page = getPageFor(cityUrlAlias,0);
        if(page==null) return "-999";
        return parser.parseCurrentGeneralCondition(page);
    }

    @Override
    public int currentTemperature(String cityUrlAlias) {
        Document page = getPageFor(cityUrlAlias, 0);
        if(page==null) return -999;
        int temperature = parser.parseCurrentTemperature(page);
        return temperature;
    }
    @Override
    public String forecastGeneralCondition(String cityUrlAlias, long plusHours) {
        Document page = getPageFor(cityUrlAlias, plusHours);
        if(page==null) return "-999";
        List<String> conditions = parser.parseGeneralConditions(page); //for given day
        int forecastHour = LocalDateTime.now().plusHours(plusHours).getHour();
        int index = locateIndexAt(page, forecastHour);
        return conditions.get(index);
    }
    @Override
    public int forecastTemperature(String cityUrlAlias, long plusHours) {
        Document page = getPageFor(cityUrlAlias, plusHours);
        if(page==null) return -999;
        List<Integer> temperatureSamples = parser.parseTemperaturesSamples(page); //for given day
        int forecastHour = LocalDateTime.now().plusHours(plusHours).getHour();
        int index = locateIndexAt(page, forecastHour);
        return temperatureSamples.get(index);
    }
    @Override
    public String getProviderName(){
        return ForecastProviderList.SINOPTIK.getName();
    }
    private int locateIndexAt(Document page, int forecastHour) {
        List<Integer> timeSamples = parser.parseWeatherTimeSamples(page);
        int index = 0;

        if (timeSamples.get(timeSamples.size() - 1) <= forecastHour) {
            index = timeSamples.size() - 1;
        } else {
            for (int i = 0; i < timeSamples.size(); i++) {
                if (timeSamples.get(i) > forecastHour) {
                    if(i == 0) break; //if first time sample is already bigger than forecast hour

                    index = i - 1;
                    break;
                }
            }
        }
        return index;
    }
}
