package com.pet.wheathercomparator.backgroundservice.collector;

import com.pet.wheathercomparator.constants.ForecastRange;
import com.pet.wheathercomparator.forecastprovider.ForecastProvider;
import com.pet.wheathercomparator.model.weather.ActualWeatherSample;
import com.pet.wheathercomparator.model.weather.ForecastWeatherSample;
import com.pet.wheathercomparator.repository.ActualWeatherRepository;
import com.pet.wheathercomparator.repository.ForecastWeatherRepository;
import com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.WeatherScraper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
@Service
@Slf4j
public class WeatherCollector {
    @Autowired
    private List<WeatherScraper> scrapers;
    @Autowired
    private ActualWeatherRepository actualRepository;
    @Autowired
    private ForecastWeatherRepository forecastRepository;
    public void saveCurrentWeather(ForecastProvider forecastProvider) {

        List<String> providerCities = forecastProvider.getMonitoredCityNames();

        for(int i = 0; i< providerCities.size(); i++){
            String city = providerCities.get(i);
            String cityUrlAlias = forecastProvider.getMonitoredCityUrlAliases().get(i);
            sampleAndSaveCurrentWeather(detectProviderScraper(forecastProvider),city,cityUrlAlias);
        }
    }
    public void saveForecastWeather(ForecastProvider forecastProvider, ForecastRange range) {
        List<String> providerCities = forecastProvider.getMonitoredCityNames();

        for(int i = 0; i< providerCities.size(); i++){
            String city = providerCities.get(i);
            String cityUrlAlias = forecastProvider.getMonitoredCityUrlAliases().get(i);
            sampleAndSaveForecastWeather(detectProviderScraper(forecastProvider),city,cityUrlAlias, range);
        }
    }
    private WeatherScraper detectProviderScraper (ForecastProvider forecastProvider){
        WeatherScraper scraper = null;
        for(WeatherScraper s : scrapers){
            if(s.getProviderName().equals(forecastProvider.getForecastProviderName()))
                scraper = s;
        }
        return scraper;
    }
    private void sampleAndSaveCurrentWeather(WeatherScraper scraper, String cityName, String cityUrlAlias) {
        double tempretature = scraper.currentTemperature(cityUrlAlias);
        if(tempretature==-999) {
            log.warn("Current weather from " + scraper.getProviderName() + " for " + cityName + " is not recorded");
            return; //was not able to reach the webpage, nothing to save
        }
        ActualWeatherSample sample = new ActualWeatherSample();

        sample.setProvider(scraper.getProviderName());
        sample.setDateTime(Timestamp.valueOf(LocalDateTime.now()));
        sample.setCity(cityName);
        sample.setTemperature(tempretature);
        sample.setGeneralCondition(scraper.currentGeneralCondition(cityUrlAlias));
        actualRepository.save(sample);
    }
    private void sampleAndSaveForecastWeather(WeatherScraper scraper, String cityName,
                                              String cityUrlAlias, ForecastRange range) {
        double tempretature = scraper.forecastTemperature(cityUrlAlias,range.getRange());
        if(tempretature==-999) {
            log.warn("Current weather from " + scraper.getProviderName() + " for " + cityName +
                    ", range=" + range.getRange() + " is not recorded");
            return; //was not able to reach the webpage, nothing to save
        }

        ForecastWeatherSample sample = new ForecastWeatherSample();
        LocalDateTime forecastOnDateTime = LocalDateTime.now().plusHours(range.getRange()); //test minutes here

        sample.setProvider(scraper.getProviderName());
        sample.setForecastDateTime(Timestamp.valueOf(forecastOnDateTime));
        sample.setForecastRange(range.getRange());
        sample.setCity(cityName);
        sample.setTemperature(tempretature);
        sample.setGeneralCondition(scraper.forecastGeneralCondition(cityUrlAlias,range.getRange()));
        sample = forecastRepository.save(sample);

        if(sample!=null){
        }
    }
}
