package com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.gismeteo;

import com.pet.wheathercomparator.backgroundservice.collector.weatherscraper.WeatherScraper;
import com.pet.wheathercomparator.constants.ForecastProviderList;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
@Slf4j
public class GismeteoScraper implements WeatherScraper {

    private GismeteoParser parser = new GismeteoParser();
    private final String baseUrl = ForecastProviderList.GISMETEO.getBaseUrl();
    private enum DayUrlSuffix {
        NOW(-1,"/now"),
        CURRENT_DAY(0, ""),
        TOMORROW(1, "/tomorrow"),
        THIRD_DAY(2, "/3-day"),
        FOURTH_DAY(3, "/4-day"),
        FIFTH_DAY(4, "/5-day"),
        SIXTH_DAY(5, "/6-day"),
        SEVENTH_DAY(6, "/7-day");
        private int plusDays;
        private String urlSuffix;
        DayUrlSuffix(int plusDays, String urlSuffix){
            this.plusDays = plusDays;
            this.urlSuffix = urlSuffix;
        }
        public static String getUrlSuffixByPlusDays(long plusDays){
            String urlSuffix = "";
            for(DayUrlSuffix day : DayUrlSuffix.values()){
                if(day.plusDays == plusDays) {
                    urlSuffix = day.urlSuffix;
                    break;
                }
            }
            return urlSuffix;
        }
    }

    private Document getPageFor(String cityUrlAlias, long hoursFromNow)  {
        Document page = null;
        String concreteUrl;
        if(hoursFromNow == 0){
            concreteUrl = this.baseUrl+cityUrlAlias+"/"
                    + DayUrlSuffix.NOW.urlSuffix;
        }
        else{
            LocalDate forecastDate = LocalDateTime.now().plusHours(hoursFromNow).toLocalDate();
            long daysBetween = ChronoUnit.DAYS.between(LocalDateTime.now().toLocalDate(), forecastDate);
            concreteUrl = this.baseUrl+cityUrlAlias+"/"
                    + DayUrlSuffix.getUrlSuffixByPlusDays(daysBetween);
        }

        try{
            page = Jsoup.connect(concreteUrl).get();
        }
        catch (IOException e){
            log.warn("Was not able to reach page: " + concreteUrl);
        }
        return page;
    }

    @Override
    public String currentGeneralCondition(String cityUrlAlias) {
        Document page = getPageFor(cityUrlAlias,0);
        if(page==null) return "-999";
        return parser.parseCurrentGeneralCondition(page);
    }

    @Override
    public int currentTemperature(String cityUrlAlias) {
        Document page = getPageFor(cityUrlAlias,0);
        if(page==null) return -999;
        return parser.parseCurrentTemperature(page);
    }
    @Override
    public String forecastGeneralCondition(String cityUrlAlias, long plusHours) {
        Document page = getPageFor(cityUrlAlias,plusHours);
        if(page==null) return "-999";
        List<String> conditions = parser.parseGeneralConditions(page); //for given day
        int forecastHour = LocalDateTime.now().plusHours(plusHours).getHour();
        int index = locateIndexAt(page, forecastHour);
        return conditions.get(index);
    }

    @Override
    public int forecastTemperature(String cityUrlAlias, long plusHours) {
        Document page = getPageFor(cityUrlAlias, plusHours);
        if(page==null) return -999;
        List<Integer> temperatureSamples = parser.parseTemperaturesSamples(page); //for given day
        int forecastHour = LocalDateTime.now().plusHours(plusHours).getHour();
        int index = locateIndexAt(page, forecastHour);
        return temperatureSamples.get(index);
    }
    @Override
    public String getProviderName() {
        return ForecastProviderList.GISMETEO.getName();
    }

    private int locateIndexAt(Document page, int forecastHour) {
        List<Integer> timeSamples = parser.parseWeatherTimeSamples(page);
        int index = 0;

        if (timeSamples.get(timeSamples.size() - 1) <= forecastHour) {
            index = timeSamples.size() - 1;
        } else {
            for (int i = 0; i < timeSamples.size(); i++) {
                if (timeSamples.get(i) > forecastHour) {
                    if(i == 0) break; //if first time sample is already bigger than forecast hour

                    index = i - 1;
                    break;
                }
            }
        }
        return index;
    }
}
