package com.pet.wheathercomparator.backgroundservice.comparator;

import com.pet.wheathercomparator.forecastprovider.ForecastProvider;
import com.pet.wheathercomparator.model.weather.ActualWeatherSample;
import com.pet.wheathercomparator.model.weather.ControlPoint;
import com.pet.wheathercomparator.model.weather.ForecastSummary;
import com.pet.wheathercomparator.model.weather.ForecastWeatherSample;
import com.pet.wheathercomparator.repository.*;
import com.pet.wheathercomparator.backgroundservice.comparator.analysismethods.ForecastAnalysis;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherComparator {
    @Autowired
    private ControlPointRepository controlRepository;
    @Autowired
    private ActualWeatherRepository actualRepository;
    @Autowired
    private ForecastWeatherRepository forecastRepository;
    @Autowired
    private ForecastSummaryRepository summaryRepository;
    @Autowired
    private ControlPointBufferRepository controlPointBufferRepository;
    @Autowired
    private ForecastSummaryBufferRepository summaryBufferRepository;
    private final int ACTUAL_SAMPLES_AVERAGING_RANGE_MIN = 95;
    private List<ForecastWeatherSample> fetchForecastSamples(Timestamp fromDateTime, String city, String source){
        List<ForecastWeatherSample> forecastSamples =
                forecastRepository
                        .findSamplesRecordedAfter(fromDateTime, city, source);
        return forecastSamples;
    }
    private List<ActualWeatherSample> fetchNewActualSamples(Timestamp fromDateTime, String city, String source){
        List<ActualWeatherSample> actualSamples =
                actualRepository.findSamplesRecordedAfter(fromDateTime, city, source);
        return actualSamples;
    }
    private List<Pair<ForecastWeatherSample, ActualWeatherSample>> pairSamples(Timestamp fromDateTime, String city, String source){
        List<Pair<ForecastWeatherSample, ActualWeatherSample>> pairs = new ArrayList<>();
        List<ForecastWeatherSample> forecastSamples = fetchForecastSamples(fromDateTime, city, source);
        List<ActualWeatherSample> actualSamples = fetchNewActualSamples(fromDateTime, city, source);

        for(int i = 0; i<forecastSamples.size(); i++){
            ForecastWeatherSample forecast = forecastSamples.get(i);
            LocalDateTime afterForecast = forecast.getForecastDateTime().
                    toLocalDateTime().plusMinutes(ACTUAL_SAMPLES_AVERAGING_RANGE_MIN);
            LocalDateTime beforeForecast = forecast.getForecastDateTime().
                    toLocalDateTime().minusMinutes(ACTUAL_SAMPLES_AVERAGING_RANGE_MIN);

            List<ActualWeatherSample> filteredActuals = filterActualsForTimeInterval(beforeForecast,
                    afterForecast, actualSamples);
            ActualWeatherSample averagedActual = averageActualSamples(filteredActuals);
            pairs.add(new Pair<>(forecast, averagedActual));
        }
        return pairs;
    }
    private List<ActualWeatherSample> filterActualsForTimeInterval(LocalDateTime from, LocalDateTime to,
                                                             List<ActualWeatherSample> actualSamples){
        List<ActualWeatherSample> selectedSamples = new ArrayList<>();

        //Select actual samples in range from-to
        for(int i = 0; i<actualSamples.size(); i++){
            ActualWeatherSample actual = actualSamples.get(i);
            LocalDateTime sampleOnDateTime = actual.getDateTime().toLocalDateTime();

            if(sampleOnDateTime.isAfter(from)
                    && sampleOnDateTime.isBefore(to)){
                selectedSamples.add(actual);
            }
        }
        return selectedSamples;
    }
    private ActualWeatherSample averageActualSamples (List<ActualWeatherSample> samples){
        if(samples.isEmpty()){
            return null;
        }
        StringBuilder concatenatedCondition = new StringBuilder(samples.get(0).getGeneralCondition());
        double avTemperature = samples.get(0).getTemperature();

        for(int i = 1; i<samples.size(); i++){
            concatenatedCondition.append(", " + samples.get(i).getGeneralCondition());
            avTemperature = avTemperature + samples.get(i).getTemperature();
        }
            avTemperature = avTemperature/samples.size();
        ActualWeatherSample averagedSample = new ActualWeatherSample();
        averagedSample.setGeneralCondition(concatenatedCondition.toString());
        averagedSample.setTemperature(avTemperature);

        return averagedSample;
    }
    public void updateControlPointsFor(ForecastProvider forecastProvider){
        List<String> cityNames = forecastProvider.getMonitoredCityNames();
        String providerName = forecastProvider.getForecastProviderName();

        for (int i=0; i<cityNames.size(); i++){
            Timestamp fromDateTime = controlRepository.findLastSavedDateTime(cityNames.get(i), providerName);
            if(fromDateTime == null) fromDateTime = new Timestamp(0);

            List<Pair<ForecastWeatherSample, ActualWeatherSample>> samplePairs =
                    pairSamples(fromDateTime, cityNames.get(i), providerName);

            for(Pair<ForecastWeatherSample, ActualWeatherSample> pair : samplePairs){
                ActualWeatherSample actual = pair.getValue1();
                ForecastWeatherSample forecast = pair.getValue0();
                if(actual == null) continue;
                ControlPoint controlPoint = createControlPoint(actual, forecast, forecastProvider);
                controlRepository.save(controlPoint);
            }
        }
    }
    public void recalcAllControlPointsToBufferTable(ForecastProvider forecastProvider){
        List<String> cityNames = forecastProvider.getMonitoredCityNames();
        String providerName = forecastProvider.getForecastProviderName();
        for (int i=0; i<cityNames.size(); i++){
            List<Pair<ForecastWeatherSample, ActualWeatherSample>> samplePairs =
                    pairSamples(new Timestamp(0), cityNames.get(i), providerName);

            for(Pair<ForecastWeatherSample, ActualWeatherSample> pair : samplePairs){
                ActualWeatherSample actual = pair.getValue1();
                ForecastWeatherSample forecast = pair.getValue0();
                if(actual == null) continue;
                ControlPoint controlPoint = createControlPoint(actual, forecast, forecastProvider);

                controlPointBufferRepository.saveToBufferTable(controlPoint);
            }
        }
    }
    private ControlPoint createControlPoint(ActualWeatherSample actual, ForecastWeatherSample forecast, ForecastProvider provider){
        ControlPoint controlPoint = new ControlPoint();

        Triplet<String, String, Double> worstCombinationInfo =
                ForecastAnalysis.getWorstActualForecastMatchAndCorrelation(forecast, actual, provider);
        controlPoint.setActualCondition(worstCombinationInfo.getValue0());
        controlPoint.setForecastCondition(worstCombinationInfo.getValue1());
        controlPoint.setConditionCorrelation(worstCombinationInfo.getValue2());

        double deltaT = ForecastAnalysis.calcTemperatureDelta(actual, forecast);
        controlPoint.setDeltaT(deltaT);

        controlPoint.setCity(forecast.getCity());
        controlPoint.setProvider(forecast.getProvider());
        controlPoint.setDateTime(forecast.getForecastDateTime());
        controlPoint.setForecastRange(forecast.getForecastRange());
        controlPoint.setControlPointId(forecast.getSampleId());

        return controlPoint;
    }
    public void updateForecastSummary(ForecastProvider forecastProvider, int onIntervalOfDays){
        Timestamp from = Timestamp.valueOf(LocalDateTime.now().minusDays(onIntervalOfDays));
        String providerName = forecastProvider.getForecastProviderName();
        List<Integer> presentRanges;

        for(String city : forecastProvider.getMonitoredCityNames()){
            presentRanges = controlRepository.findDistinctRangesFromDate(city, providerName, from);
            for(int range : presentRanges){
                List<ControlPoint> subset = controlRepository.findControlPointsFromDate(
                        from, city, providerName, range);
                if (subset.size() == 1) continue;
                ForecastSummary summary = createForecastSummary(subset, city, range, providerName, onIntervalOfDays);
                summaryRepository.save(summary);
            }
        }
    }
    public void recalcAllSummaryToBufferTable(ForecastProvider forecastProvider, int onIntervalOfDays){
        Timestamp from = Timestamp.valueOf(LocalDateTime.now().minusDays(onIntervalOfDays));
        String providerName = forecastProvider.getForecastProviderName();
        List<Integer> presentRanges;

        for(String city : forecastProvider.getMonitoredCityNames()){
            presentRanges = controlRepository.findDistinctRangesFromDate(city, providerName, from);
            for(int range : presentRanges){
                List<ControlPoint> subset = controlRepository.findControlPointsFromDate(
                        from, city, providerName, range);
                if(subset.size()==1) continue;
                ForecastSummary summary = createForecastSummary(subset, city, range, providerName, onIntervalOfDays);
                summaryBufferRepository.saveToBufferTable(summary);
            }
        }
    }
    public ForecastSummary createForecastSummary(List<ControlPoint> subset, String city,
                                                 int range, String providerName, int onIntervalOfDays){
        ForecastSummary summary = new ForecastSummary();
        double conditionRTD = ForecastAnalysis.calcConditionRsdAsPercentage(subset);
        double temperatureRTD = ForecastAnalysis.calcTemperatureRSD(subset);

        summary.setConditionCorrelationRSD(conditionRTD);
        summary.setTemperatureRSD(temperatureRTD);
        summary.setCity(city);
        summary.setDate(Timestamp.valueOf(LocalDateTime.now()));
        summary.setOnIntervalOfDays(onIntervalOfDays);
        summary.setProvider(providerName);
        summary.setForecastRange(range);

        return summary;
    }
}
