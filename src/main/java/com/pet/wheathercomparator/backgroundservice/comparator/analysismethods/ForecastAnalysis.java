package com.pet.wheathercomparator.backgroundservice.comparator.analysismethods;

import com.pet.wheathercomparator.forecastprovider.ForecastProvider;
import com.pet.wheathercomparator.model.weather.ActualWeatherSample;
import com.pet.wheathercomparator.model.weather.ControlPoint;
import com.pet.wheathercomparator.model.weather.ForecastWeatherSample;
import org.javatuples.Triplet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class ForecastAnalysis {
    public static double calcTemperatureDelta(ActualWeatherSample actual, ForecastWeatherSample forecast) {
        return actual.getTemperature() - forecast.getTemperature();
    }
    public static Triplet<String, String, Double> getWorstActualForecastMatchAndCorrelation(
            ForecastWeatherSample forecast, ActualWeatherSample actual, ForecastProvider forecastProvider){

        String forecastCondition = forecast.getGeneralCondition().toLowerCase();
        String actualCondition = actual.getGeneralCondition().toLowerCase();
        List<String> providerConditions = forecastProvider.getProviderConditionNames();
        List<String> conventionalConditions = forecastProvider.getConventionalConditionNames();

        List<List<Double>> correlationCoeffs = forecastProvider.getCorrelationMatrix();

        double corrCoef = 1;
        String worstActual = "";
        String worstForecast = "";
        for (int i=0; i<providerConditions.size(); i++) {
            if (actualCondition.contains(providerConditions.get(i))) {
                for (int j = 0; j < providerConditions.size(); j++) {
                    if (forecastCondition.contains(providerConditions.get(j))
                            & corrCoef >= correlationCoeffs.get(i).get(j)) {
                        corrCoef = correlationCoeffs.get(i).get(j);
                        worstActual = conventionalConditions.get(i);
                        worstForecast = conventionalConditions.get(j);
                    }
                }
            }
        }
        return new Triplet<>(worstActual, worstForecast, convertCorrelationToAccuracy(corrCoef));
    }

    //modified relative standard deviation, with average=1 as perfect forecast
    public static double calcConditionRsdAsPercentage(List<ControlPoint> controlPoints){
        List<Double> subset = new ArrayList<>();
        for(ControlPoint controlPoint : controlPoints){
            subset.add(controlPoint.getConditionCorrelation());
        }
        double variation = 0;
        double average = 100; //perfect forecast
        for(double value : subset){
            variation = variation + Math.pow(value-average, 2);
        }
        variation = variation/(subset.size()-1);

        double rsd = Math.pow(variation, 0.5);
        return rsd;
    }
    public static double calcTemperatureRSD(List<ControlPoint> controlPoints){
        List<Double> subset = new ArrayList<>();
        for(ControlPoint controlPoint : controlPoints){
            subset.add(controlPoint.getDeltaT());
        }
        double variation = 0;
        double average = 0; //perfect forecast
        for(double value : subset){
            variation = variation + Math.pow(value-average, 2);
        }
        variation = variation/(subset.size()-1);

        double rsd = Math.pow(variation, 0.5);
        return rsd;
    }

    //convert correlation coefficient to 0-100% accuracy
    private static double convertCorrelationToAccuracy(double correlation){
        double accuracy;

        if(correlation>0) accuracy = ((correlation/2 + 0.5)*100);
        else accuracy = (0.5 - Math.abs(correlation)/2)*100;

        return accuracy;
    }
}
