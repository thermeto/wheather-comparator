package com.pet.wheathercomparator.backgroundservice;

import com.pet.wheathercomparator.constants.ForecastRange;
import com.pet.wheathercomparator.constants.SummaryTableInterval;
import com.pet.wheathercomparator.forecastprovider.ForecastProvider;
import com.pet.wheathercomparator.backgroundservice.collector.WeatherCollector;
import com.pet.wheathercomparator.backgroundservice.comparator.WeatherComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BackgroundExecutor {
    @Autowired
    private List<ForecastProvider> providers;
    @Autowired
    private WeatherComparator weatherComparator;
    @Autowired
    private WeatherCollector weatherCollector;

    private List<ForecastRange> forecastRanges = Arrays.asList(
            ForecastRange.THREEHOUR, ForecastRange.SIXHOUR,
            ForecastRange.TWELVEHOUR, ForecastRange.ONEDAY,
            ForecastRange.TWODAY, ForecastRange.THREEDAY,
            ForecastRange.FOURDAY, ForecastRange.FIVEDAY,
            ForecastRange.SIXDAY
    );
    private List<SummaryTableInterval> summaryTableIntervals = Arrays.asList(
            SummaryTableInterval.PREV_DAY,
            SummaryTableInterval.LAST_3_DAYS,
            SummaryTableInterval.LAST_WEEK,
            SummaryTableInterval.LAST_2_WEEKS,
            SummaryTableInterval.LAST_MONTH
    );

    //@Scheduled(fixedDelayString = "PT1H")
    public void saveCurrentWeather() {
        for (ForecastProvider provider : providers) {
            weatherCollector.saveCurrentWeather(provider);
        }
    }

    //@Scheduled(fixedDelayString = "PT3H")
    public void forecastWeather() {
        for (ForecastProvider provider : providers) {
            for(ForecastRange range : forecastRanges)
                weatherCollector.saveForecastWeather(provider, range);
        }
    }
    //@Scheduled(fixedDelayString = "PT6H")
    public void updateControlPoints() {
        for (ForecastProvider provider : providers) {
            weatherComparator.updateControlPointsFor(provider);
        }
    }

    //@Scheduled(fixedDelayString = "PT23H")
    public void updateForecastSummary() {
        for (ForecastProvider provider : providers) {
            for(SummaryTableInterval interval : summaryTableIntervals)
                weatherComparator.updateForecastSummary(provider, interval.getInterval());
        }
    }
    public void recalcControlPointsToBufferTable() {
        for (ForecastProvider provider : providers) {
            weatherComparator.recalcAllControlPointsToBufferTable(provider);
        }
    }
    public void recalcForecastSummaryToBufferTable() {
        for (ForecastProvider provider : providers) {
            for(SummaryTableInterval interval : summaryTableIntervals)
                weatherComparator.recalcAllSummaryToBufferTable(provider, interval.getInterval());
        }
    }
}
