package com.pet.wheathercomparator.validation;

import com.pet.wheathercomparator.annotation.UniqueEmail;
import com.pet.wheathercomparator.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {
    @Autowired
    PersonRepository repository;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(repository.findByEmail(s) != null) return false;
        return true;
    }
}
