package com.pet.wheathercomparator.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name = "roles")
public class Role {

        @Id
        @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
        @GenericGenerator(name = "native",strategy = "native")
        private int roleId;

        @Column(name = "role_name")
        private String roleName;
}
