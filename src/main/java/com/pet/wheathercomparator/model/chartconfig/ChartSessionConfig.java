package com.pet.wheathercomparator.model.chartconfig;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.constants.ForecastRange;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "session",proxyMode = ScopedProxyMode.TARGET_CLASS)
@Getter
@Setter
public class ChartSessionConfig extends ChartGlobalConfig {
    private List<String> selectedProviders;
    private String selectedRange;
    private String selectedCity;
    private String selectedAnalysisType;
    private String units;
    public ChartSessionConfig(){
        selectedProviders = new ArrayList<>();
        selectedProviders.add(super.getListOfProviders()[0]);
        selectedCity = super.getListOfCities()[0];
        selectedAnalysisType = AnalysisType.GENERAL_CONDITION.getAnalysisType();
        units = AnalysisType.GENERAL_CONDITION.getUnits();
        selectedRange = ForecastRange.THREEHOUR.getDisplayName();
    }
}
