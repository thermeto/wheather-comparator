package com.pet.wheathercomparator.model.chartconfig;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.constants.CityList;
import com.pet.wheathercomparator.constants.ForecastProviderList;
import com.pet.wheathercomparator.constants.ForecastRange;
import lombok.Getter;
@Getter
//this options will be displayed in chart configuration menu
public abstract class ChartGlobalConfig {
    public final String[] listOfProviders = new String[]{ForecastProviderList.SINOPTIK.getName(),
            ForecastProviderList.GISMETEO.getName()};
    public final String[] listOfCities = new String[]{
            CityList.KYIV.getCity(),
            CityList.KHARKIV.getCity(),
            CityList.LVIV.getCity(),
            CityList.ZHYTOMYR.getCity(),
            CityList.ODESA.getCity(),
    };
    public final String[] listOfRanges = new String[]{
            ForecastRange.THREEHOUR.getDisplayName(),
            ForecastRange.SIXHOUR.getDisplayName(),
            ForecastRange.TWELVEHOUR.getDisplayName(),
            ForecastRange.ONEDAY.getDisplayName(),
            ForecastRange.TWODAY.getDisplayName(),
            ForecastRange.THREEDAY.getDisplayName()
    };
    public final String[] listOfAnalysisTypes = new String[]{
            AnalysisType.TEMPERATURE.getAnalysisType(),
            AnalysisType.GENERAL_CONDITION.getAnalysisType()
    };
}
