package com.pet.wheathercomparator.model.correlationmatrix;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;
@Entity
@Table(name="condition_matrix")
@Getter
@Setter
public class ConditionDefinition {
    @Id
    private int severityLevel;
    private String conditionName;
    @Max(value = 1)
    @Min(value = -1)
    private double sunny;
    @Max(value = 1)
    @Min(value = -1)
    private double partlyCloudy;
    @Max(value = 1)
    @Min(value = -1)
    private double cloudyWithClearings;
    @Max(value = 1)
    @Min(value = -1)
    private double cloudy;
    @Max(value = 1)
    @Min(value = -1)
    private double rain;
    @Max(value = 1)
    @Min(value = -1)
    private double thunder;
    public List<Double> getCoefficientsList(){
        return List.of(sunny, partlyCloudy, cloudyWithClearings, cloudy, rain, thunder);
    }
    public void mapListToCorrelationCoefficients(List<Double> correlationCoefficients){
        this.sunny = correlationCoefficients.get(0);
        this.partlyCloudy = correlationCoefficients.get(1);
        this.cloudyWithClearings = correlationCoefficients.get(2);
        this.cloudy = correlationCoefficients.get(3);
        this.rain = correlationCoefficients.get(4);
        this.thunder = correlationCoefficients.get(5);
    }
    //defined in dashboard
    @Transient
    @Max(value = 1, message="Coefficient must be <=1")
    @Min(value = -1, message="Coefficient must be >=-1")
    private List<Double> newCorrelationCoefficients;

    @Override
    public boolean equals(Object o) {
        ConditionDefinition cd = (ConditionDefinition) o;
        if(this.getClass()!=o.getClass()) return false;
        else if(!this.getConditionName().equals(cd.getConditionName())) return false;
        else if(!this.getCoefficientsList().equals(cd.getCoefficientsList())) return false;
        return true;
    }
}
