package com.pet.wheathercomparator.model.correlationmatrix;

import lombok.Data;

import java.util.List;

@Data
public class ConditionMatrix {
    private List<ConditionDefinition> matrix;

    @Override
    public boolean equals (Object o) {
        ConditionMatrix cm = (ConditionMatrix) o;
        for(int i=0; i<matrix.size(); i++){
            ConditionDefinition cd1 = cm.getMatrix().get(i);
            ConditionDefinition cd2 = this.getMatrix().get(i);
            if(!cd1.equals(cd2)) return false;
        }
        return true;
    }
}
