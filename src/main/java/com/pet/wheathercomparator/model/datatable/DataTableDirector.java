package com.pet.wheathercomparator.model.datatable;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.constants.SummaryTableInterval;
import com.pet.wheathercomparator.model.datatable.builder.DataTableBuilder;
import com.pet.wheathercomparator.model.weather.ControlPoint;
import com.pet.wheathercomparator.repository.ControlPointRepository;
import com.pet.wheathercomparator.repository.ForecastSummaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class DataTableDirector {
    final int RECORDS_TO_DISPLAY = 120;
    @Autowired
    ControlPointRepository controlRepository;
    @Autowired
    ForecastSummaryRepository summaryRepository;
    public void prepareTemperatureChart(DataTableBuilder builder, String city, List<String> selectedProviders, int range){

        List<Timestamp> dates = getDatesWithLongerHistory(city, selectedProviders, range);
        builder.addXAxisValues(new ArrayList<>(dates));

        for(int i=0; i<selectedProviders.size(); i++){
            List<Object> columnValues = new ArrayList<>();
            List<ControlPoint> controlPoints = controlRepository
                    .findLastControlPoints(city, selectedProviders.get(i), range, RECORDS_TO_DISPLAY);
            int index = 0;
            //for several functions align X-axis values (x-value for one function must be close to x-value of another)
            for(int j=0; j<dates.size(); j++){
                if(index >= controlPoints.size()) {
                    columnValues.add(null);
                    continue;
                }
                ControlPoint cp = controlPoints.get(index);
                if(isWithin30minRange(dates.get(j), cp.getDateTime())){
                    columnValues.add(cp.getDeltaT());
                    index++;
                }
                else columnValues.add(null);
            }
            builder.addYAxisValues(new ArrayList<>(columnValues));
        }
    }

    //dataTable consists of x-axis column (dates) and 3 columns for each provider data:
    //1) column with Y-axis values
    //2) column with annotation symbol (always visible on the chart, when hovered it shows 3d column)
    //3) column with additional info
    public void prepareConditionChart(DataTableBuilder builder, String city, List<String> selectedProviders, int range){
        String annotationSymbol;
        if(selectedProviders.size()==1){
            annotationSymbol = "◉";
        }
        else annotationSymbol="";

        List<Timestamp> dates = getDatesWithLongerHistory(city, selectedProviders, range);
        builder.addXAxisValues(new ArrayList<>(dates));

        for(int i=0; i<selectedProviders.size(); i++){
            List<Object> columnValues = new ArrayList<>();
            List<ControlPoint> controlPoints = controlRepository
                    .findLastControlPoints(city, selectedProviders.get(i), range, RECORDS_TO_DISPLAY);
            int index = 0;
            //for several functions align X-axis values (x-value for one function must be close to x-value of another)
            for(int j=0; j<dates.size(); j++){
                if(index >= controlPoints.size()) {
                    columnValues.add(null);
                    continue;
                }
                ControlPoint cp = controlPoints.get(index);
                if(isWithin30minRange(dates.get(j), cp.getDateTime())) {
                    columnValues.add(cp.getConditionCorrelation());
                    index++;
                }
                else columnValues.add(null);
            }

            builder.addYAxisValues(columnValues);

            if(selectedProviders.size()==1){
                List<Object> annotationColumn = Collections.nCopies(dates.size(), annotationSymbol);
                builder.addYAxisValues(annotationColumn);
                columnValues = new ArrayList<>(controlRepository.findLastConditionNames
                        (city, selectedProviders.get(i), range, RECORDS_TO_DISPLAY));
                builder.addYAxisValues(columnValues);
            }
        }
    }
    public void prepareSummaryTable(DataTableBuilder builder, String city, List<String> selectedProviders,
                                    String units, int range, String analysisType){
        List<Object> summaryTableHeaders = new ArrayList<>();
        summaryTableHeaders.add("Provider");

        //get intervals to display (can be changed in enum)
        List<Integer> rsdOnIntervals = new ArrayList<>();
        for(SummaryTableInterval interval : SummaryTableInterval.values()){
            summaryTableHeaders.add(interval.getIntervalHeader());
            rsdOnIntervals.add(interval.getInterval());
        }

        builder.addXAxisValues(summaryTableHeaders);

        for(int i=0; i<selectedProviders.size(); i++){
            List<Object> rowValues = new ArrayList<>();
            rowValues.add(selectedProviders.get(i));
            List<Double> rsds = getRelevantRsd(selectedProviders.get(i), rsdOnIntervals, range, analysisType, city);

            for(int j=0; j<rsdOnIntervals.size(); j++){
                rowValues.add(
                        j>=rsds.size() ? "Not ready yet" : rsds.get(j).toString() + units);
            }
            builder.addYAxisValues(rowValues);
        }
    }

    private boolean isWithin30minRange(Timestamp referenceDate, Timestamp checkDate) {
        var reference = referenceDate.toLocalDateTime();
        var check = checkDate.toLocalDateTime();
        return !(check.isBefore(reference.minusMinutes(30))
                || check.isAfter(reference.plusMinutes(30)));
    }
    private List<Timestamp> getDatesWithLongerHistory (String city, List<String> selectedProviders, int range) {
        int noOfRecordsToFetch = 120;

        List<Timestamp> maxSizeDates = new ArrayList<>();
        int maxSize = 0;
        for (String provider : selectedProviders) {
            List<Timestamp> dates = controlRepository
                    .findLastRecordedDates(city, provider, range, noOfRecordsToFetch);
            if (maxSize < dates.size()) {
                maxSizeDates = dates;
                maxSize = dates.size();
            }
        }
        return maxSizeDates;
    }

    private List<Double> getRelevantRsd (String provider, List<Integer> onIntervals,
                                         int range, String analysisType, String city){
        List<Double> rsds;

        if(analysisType.equals(AnalysisType.GENERAL_CONDITION.getAnalysisType())){
            rsds = summaryRepository
                    .findLastConditionCorrelationRSD(city, provider, range, onIntervals);
        }
        else{
            rsds = summaryRepository.findLastTemperatureRSD(city, provider, range, onIntervals);
        }
        return rsds;
    }
}
