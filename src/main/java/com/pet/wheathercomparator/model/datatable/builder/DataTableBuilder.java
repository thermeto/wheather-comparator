package com.pet.wheathercomparator.model.datatable.builder;

import java.util.List;

public interface DataTableBuilder {
    public void addXAxisValues(List<Object> headers);
    public void addYAxisValues(List<Object> values);
    public List<List<Object>> getTable();
}
