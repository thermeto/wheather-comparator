package com.pet.wheathercomparator.model.datatable.builder;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class StandardTableBuilder implements DataTableBuilder{

    private List<List<Object>> dataTable;

    @Override
    public void addXAxisValues(List<Object> headers) {
        this.dataTable = new ArrayList<>();
        this.dataTable.add(headers);
    }

    @Override
    public void addYAxisValues(List<Object> values) {
        this.dataTable.add(values);
    }

    @Override
    public List<List<Object>> getTable(){
        return this.dataTable;
    }
}
