package com.pet.wheathercomparator.model.datatable.builder;

import lombok.Getter;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

@Getter
public class ChartDataTableBuilder implements DataTableBuilder{
    private List<List<Object>> dataTable;
    @Override
    public void addXAxisValues(List<Object> values) {
        List<List<Object>> localDataTable = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM", Locale.US);
        int numberOfPoints = values.size();
        if(numberOfPoints > 120)  numberOfPoints = 120;

        for (int i=0; i< numberOfPoints; i++){
            localDataTable.add(new ArrayList<>());
        }

        for(int i=0; i< numberOfPoints; i++){
            if(i%8==0){
                Timestamp dateTime = (Timestamp) values.get(i);
                localDataTable.get(i).add(dateTime.toLocalDateTime()
                        .toLocalDate().format(formatter));
                continue;
            }
            localDataTable.get(i).add(null);
        }
        this.dataTable = localDataTable;
    }
    @Override
    public void addYAxisValues(List<Object> values) {
        List<List<Object>> localDataTable = new ArrayList<>();
        if(this.dataTable == null) {
            for (int i=0; i< values.size(); i++){
                localDataTable.add(new ArrayList<>());
            }
        }
        else localDataTable = this.dataTable;

        //add new values from the end (if recording of some samples started recently)
        int YvaluesIndex = values.size()-1;
        for(int i=localDataTable.size()-1; i>=0; i--){
            //if new column has less values than values on X axis then add nulls
            if(YvaluesIndex<0)
            {
                localDataTable.get(i).add(null);
                continue;
            }
            localDataTable.get(i).add(values.get(YvaluesIndex));
            YvaluesIndex--;
        }
        this.dataTable = localDataTable;
    }

    @Override
    public List<List<Object>> getTable(){
        return this.dataTable;
    }
}
