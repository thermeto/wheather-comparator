package com.pet.wheathercomparator.model.weather;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="forecast_summary")
@Getter
@Setter
public class ForecastSummary {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
    @GenericGenerator(name = "native",strategy = "native")
    private int id;
    private Timestamp date;
    private String city;
    private String provider;
    @Column(name="forecast_range")
    private int forecastRange;
    @Column(name="on_interval_of_days")
    private int onIntervalOfDays;
    @Column(name="condition_correlation_rsd")
    private double conditionCorrelationRSD;
    @Column(name="temperature_rsd")
    private double temperatureRSD;
}
