package com.pet.wheathercomparator.model.weather;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name="forecast_weather_samples")
public class ForecastWeatherSample {
    @Id
    @Column(name="sample_id")
    @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
    @GenericGenerator(name = "native",strategy = "native")
    private int sampleId;
    @Column(name="forecast_range")
    private int forecastRange;
    @Column(name="forecast_date_time")
    private Timestamp forecastDateTime;

    private String provider;
    private String city;
    @Column(name="general_condition")
    private String generalCondition;
    private double temperature;
}
