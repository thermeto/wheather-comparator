package com.pet.wheathercomparator.model.weather;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name="actual_weather_samples")
public class ActualWeatherSample {

    @Id
    @Column(name="sample_id")
    @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
    @GenericGenerator(name = "native",strategy = "native")
    private int sampleId;

    @Column(name="date_time")
    private Timestamp dateTime;

    private String provider;

    private String city;

    @Column(name="general_condition")
    private String generalCondition;
    private double temperature;
}
