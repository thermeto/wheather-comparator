package com.pet.wheathercomparator.model.weather;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="control_points")
@Getter
@Setter
public class ControlPoint {
    @Id
    @Column(name="control_id")
    //@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST, targetEntity = ForecastWeatherSample.class)
    //@JoinColumn(name="control_id", referencedColumnName = "sample_id")
    private int controlPointId;
    @Column(name="date_time")
    private Timestamp dateTime;
    @Column(name="forecast_range")
    private int forecastRange;
    @Column(name="delta_t")
    private double deltaT;
    @Column(name="condition_corr")
    private double conditionCorrelation;
    @Column(name="actual_condition")
    private String actualCondition;
    @Column(name="forecast_condition") //make one-to-one
    private String forecastCondition;
    private String provider;
    private String city;

}
