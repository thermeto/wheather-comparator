package com.pet.wheathercomparator.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name="proposals")
@Getter
@Setter
public class Proposal {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
    @GenericGenerator(name = "native",strategy = "native")
    private int id;
    @Column(name="date_time")
    private Timestamp dateTime;
    private String type;

    @Size(min=3, max=40, message="Name must be 3..100 characters long")
    private String name;
    @Email(message = "Please provide a valid email address" )
    private String email;
    @NotBlank(message="Subject must not be blank")
    @Size(min=4, max=40, message="Subject must be 4..40 characters long")
    private String subject;

    @NotBlank(message="Details must not be blank")
    @Size(min=10, max=1500, message="Message must be 10..1500 characters long")
    private String details;
}
