package com.pet.wheathercomparator.service;

import com.pet.wheathercomparator.constants.AnalysisType;
import com.pet.wheathercomparator.constants.ForecastRange;
import com.pet.wheathercomparator.model.chartconfig.ChartSessionConfig;
import com.pet.wheathercomparator.model.Person;
import com.pet.wheathercomparator.model.Proposal;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.model.datatable.DataTableDirector;
import com.pet.wheathercomparator.model.datatable.builder.ChartDataTableBuilder;
import com.pet.wheathercomparator.model.datatable.builder.DataTableBuilder;
import com.pet.wheathercomparator.model.datatable.builder.StandardTableBuilder;
import com.pet.wheathercomparator.repository.ConditionMatrixRepository;
import com.pet.wheathercomparator.repository.PersonRepository;
import com.pet.wheathercomparator.repository.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class HomeService {
    @Autowired
    private ProposalRepository proposalRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ChartSessionConfig sessionConfig;
    @Autowired
    private DataTableDirector dataTableDirector;
    @Autowired
    private ConditionMatrixRepository conditionMatrixRepository;
    public List<List<Object>> buildChartDataTable(){
        String city = this.sessionConfig.getSelectedCity();
        List<String> selectedProviders = this.sessionConfig.getSelectedProviders();
        int range = ForecastRange.getRangeByDisplayName(this.sessionConfig.getSelectedRange());

        DataTableBuilder chartBuilder = new ChartDataTableBuilder();

        if(sessionConfig.getSelectedAnalysisType().equals(AnalysisType.GENERAL_CONDITION.getAnalysisType())){
            dataTableDirector.prepareConditionChart(chartBuilder, city, selectedProviders, range);
        }
        else{
            dataTableDirector.prepareTemperatureChart(chartBuilder, city, selectedProviders, range);
        }
        return chartBuilder.getTable();
    }
    public List<List<Object>> buildSummaryDataTable(){
        String city = this.sessionConfig.getSelectedCity();
        List<String> selectedProviders = this.sessionConfig.getSelectedProviders();
        int range = ForecastRange.getRangeByDisplayName(this.sessionConfig.getSelectedRange());
        String analysisType = sessionConfig.getSelectedAnalysisType();

        DataTableBuilder chartBuilder = new StandardTableBuilder();
        String units = AnalysisType.getUnitsByAnalysisType(analysisType);
        dataTableDirector.prepareSummaryTable(chartBuilder, city, selectedProviders, units,range, analysisType);

        return chartBuilder.getTable();
    }
    public void saveProposal(Proposal proposal){
        proposal.setDateTime(Timestamp.valueOf(LocalDateTime.now()));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserEmail = authentication.getName();

            Person person = personRepository.findByEmail(currentUserEmail);
            proposal.setEmail(currentUserEmail);
            proposal.setName(person.getName());
        }
        proposalRepository.save(proposal);
    }
    public ConditionMatrix getConditionCorrelationsFullList() {
        ConditionMatrix matrix = new ConditionMatrix();
        matrix.setMatrix(conditionMatrixRepository.readCorrelationCoefficients());
        return matrix;
    }

    public User mapPersonToUser(String currentUserEmail){
        Person person = personRepository.findByEmail(currentUserEmail);

        User user = new User();
        user.setCity(person.getCity());
        user.setName(person.getName());
        user.setEmail(person.getEmail());
        user.setId(person.getId());
        user.setFavoriteProvider(person.getFavoriteProvider());

        return user;
    }
}
