package com.pet.wheathercomparator.service;

import com.pet.wheathercomparator.model.Person;
import com.pet.wheathercomparator.model.Role;
import com.pet.wheathercomparator.model.User;
import com.pet.wheathercomparator.repository.PersonRepository;
import com.pet.wheathercomparator.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    public boolean createNewPerson(Person person){
        boolean isSaved = false;
        Role role = roleRepository.getByRoleName("USER");
        person.setRole(role);
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        person = personRepository.save(person);
        if (null != person && person.getId() > 0)
        {
            isSaved = true;
        }
        return isSaved;
    }

    public void updatePerson(User user){
        Person person = personRepository.findById(user.getId());
        person.setName(user.getName());
        person.setEmail(user.getEmail());
        person.setFavoriteProvider(user.getFavoriteProvider());
        person.setCity(user.getCity());

        personRepository.save(person);
    }
}
