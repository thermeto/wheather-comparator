package com.pet.wheathercomparator.service;

import com.pet.wheathercomparator.backgroundservice.BackgroundExecutor;
import com.pet.wheathercomparator.forecastprovider.ForecastProvider;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionDefinition;
import com.pet.wheathercomparator.model.correlationmatrix.ConditionMatrix;
import com.pet.wheathercomparator.repository.ConditionMatrixRepository;
import com.pet.wheathercomparator.repository.ControlPointBufferRepository;
import com.pet.wheathercomparator.repository.ForecastSummaryBufferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class DashboardService {
    private ConditionMatrixRepository conditionMatrixRepository;
    private final ReentrantLock matrixRecalcLock = new ReentrantLock();
    @Autowired
    ControlPointBufferRepository controlPointBufferRepository;
    @Autowired
    ForecastSummaryBufferRepository summaryBufferRepository;
    @Autowired
    private BackgroundExecutor executor;
    @Autowired
    public final void setCorrelationMatrixRepository(ConditionMatrixRepository conditionMatrixRepository) {
        this.conditionMatrixRepository = conditionMatrixRepository;
    }
    public ConditionMatrix getConditionCorrelationsFullList() {
        ConditionMatrix matrix = new ConditionMatrix();
        matrix.setMatrix(conditionMatrixRepository.readCorrelationCoefficients());
        return matrix;
    }
    public void updateCorrelationMatrixToDB(ConditionMatrix matrix) {
        List<List<Double>> corrMatrix = mirrorCorrelationMatrix(matrix.getMatrix());
        for (int i = 0; i < corrMatrix.size(); i++) {
            ConditionDefinition condition = matrix.getMatrix().get(i);
            condition.mapListToCorrelationCoefficients(corrMatrix.get(i));
            condition.setSeverityLevel(i + 1);
            conditionMatrixRepository.save(condition);
            ForecastProvider.updateLocalCorrelationMatrix();
        }
    }
    private List<List<Double>> mirrorCorrelationMatrix(List<ConditionDefinition> conditionMatrix) {
        //mirror defined coefficients to bottom part of the matrix
        List<List<Double>> corrMatrix = new ArrayList<>();
        for (int i = 0; i < conditionMatrix.size(); i++) {
            List<Double> matrixRow = conditionMatrix.get(i).getNewCorrelationCoefficients();

            for (int j = 0; j < conditionMatrix.size(); j++) {
                //if matrix axis
                if (i == j) matrixRow.set(j, 1.0);
                    //mirror side
                else if (j < i) matrixRow.set(j, corrMatrix.get(j).get(i));
                else break;
            }
            corrMatrix.add(matrixRow);
        }
        return corrMatrix;
    }
    public boolean inputIsValid(ConditionMatrix matrix) {
        List<List<Double>> corrMatrix = mirrorCorrelationMatrix(matrix.getMatrix());
        for (List<Double> row : corrMatrix) {
            for (double coeff : row) {
                if (coeff > 1 || coeff < -1) return false;
            }
        }
        return true;
    }
    public void applyNewCoefficientsToMatrixForm(ConditionMatrix matrix) {
        for (ConditionDefinition condition : matrix.getMatrix()) {
            condition.mapListToCorrelationCoefficients(condition.getNewCorrelationCoefficients());
        }
    }
    public boolean tryLaunchForecastAnalysisWithNewCoeffs(ConditionMatrix matrix) {
        boolean isSuccessful = false;
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                if (matrixRecalcLock.tryLock()) {
                    try {
                        updateCorrelationMatrixToDB(matrix);
                        prepareBufferTables();
                        recalcControlPointsAndPush();
                        recalcForecastSummaryAndPush();
                    } finally {
                        matrixRecalcLock.unlock();
                    }
                }
            }
        });
        if(!matrixRecalcLock.isLocked()){
            t1.start();
            isSuccessful = true;
        }
        return isSuccessful;
    }
    private void prepareBufferTables(){
        controlPointBufferRepository.prepareBufferTable();
        summaryBufferRepository.prepareBufferTable();
    }
    private void recalcControlPointsAndPush() {
        executor.recalcControlPointsToBufferTable();
        controlPointBufferRepository.replaceOriginalTableWithBuffer();
    }

    private void recalcForecastSummaryAndPush() {
        executor.recalcForecastSummaryToBufferTable();
        summaryBufferRepository.replaceOriginalTableWithBuffer();
    }

}