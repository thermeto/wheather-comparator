INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('sunny', 1, 0.8, 0.7,	0.5, -0.2, -1);

INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('partly cloudy', 0.8, 1, 0.8,	0.7, 0.2, -0.7);

INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('cloudy with clearings', 0.7, 0.8, 1,	0.8, 0.4, -0.5);

INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('cloudy', 0.5, 0.7, 0.8,	1, 0.6, -0.3);

INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('rain', -0.2, 0.2, 0.4,	0.6, 1, 0.8);

INSERT INTO condition_matrix (condition_name, sunny, partly_cloudy, cloudy_with_clearings, cloudy, rain, thunder)
VALUES ('thunder', -1, -0.7, -0.5,	-0.3, 0.8, 1);

SELECT * FROM condition_matrix;
INSERT INTO sinoptik_condition_names (severity_level, condition_name)
VALUES (1, 'ясно');

INSERT INTO sinoptik_condition_names (severity_level,condition_name)
VALUES (2, 'невелика хмарність');

INSERT INTO sinoptik_condition_names (severity_level,condition_name)
VALUES (3, 'мінлива хмарність');
INSERT INTO sinoptik_condition_names (severity_level,condition_name)
VALUES (4, 'хмарно');

INSERT INTO sinoptik_condition_names (severity_level,condition_name)
VALUES (5, 'дощ');

INSERT INTO sinoptik_condition_names (severity_level,condition_name)
VALUES (6, 'грози');


INSERT INTO gismeteo_condition_names (severity_level, condition_name)
VALUES (1, 'ясно');

INSERT INTO gismeteo_condition_names (severity_level,condition_name)
VALUES (2, 'слабка хмарність');

INSERT INTO gismeteo_condition_names (severity_level,condition_name)
VALUES (3, 'хмарно');
INSERT INTO gismeteo_condition_names (severity_level,condition_name)
VALUES (4, 'сильна хмарність');

INSERT INTO gismeteo_condition_names (severity_level,condition_name)
VALUES (5, 'дощ');

INSERT INTO gismeteo_condition_names (severity_level,condition_name)
VALUES (6, 'гроза');

INSERT INTO roles (role_id,role_name)
VALUES (1, 'USER');

INSERT INTO roles (role_id,role_name)
VALUES (2, 'ADMIN');