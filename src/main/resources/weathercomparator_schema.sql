CREATE TABLE actual_weather_samples (
  sample_id INT AUTO_INCREMENT PRIMARY KEY,
  date_time TIMESTAMP NOT NULL,
  provider VARCHAR(40) NOT NULL,
  city VARCHAR(40) NOT NULL,
  general_condition VARCHAR(100) DEFAULT NULL,
  temperature DECIMAL(3,1) DEFAULT 0
);

CREATE TABLE forecast_weather_samples (
  sample_id INT AUTO_INCREMENT PRIMARY KEY,
  forecast_range INT DEFAULT 1,
  forecast_date_time TIMESTAMP NOT NULL,
  web_source VARCHAR(40) NOT NULL,
  city VARCHAR(40) NOT NULL,
  general_condition VARCHAR(40) DEFAULT NULL,
  temperature DECIMAL(3,1) DEFAULT NULL,
  precipitation DECIMAL(6,3) DEFAULT NULL
);

CREATE TABLE control_points (
  control_id INT,
  date_time TIMESTAMP DEFAULT NULL,
  forecast_range INT DEFAULT 0,
  provider VARCHAR(40) DEFAULT NULL,
  city VARCHAR(40) DEFAULT NULL,
  condition_corr DECIMAL(6,3) DEFAULT 0,
  delta_t DECIMAL(3,1) DEFAULT 1,
  actual_condition VARCHAR(40) DEFAULT NULL,
  forecast_condition VARCHAR(100) DEFAULT NULL,
  FOREIGN KEY (`control_id`)
REFERENCES forecast_weather_samples(`sample_id`)
);

CREATE TABLE control_points_buffer (
  control_id INT,
  date_time TIMESTAMP DEFAULT NULL,
  forecast_range INT DEFAULT 0,
  provider VARCHAR(40) DEFAULT NULL,
  city VARCHAR(40) DEFAULT NULL,
  condition_corr DECIMAL(6,3) DEFAULT 0,
  delta_t DECIMAL(3,1) DEFAULT 1,
  actual_condition VARCHAR(40) DEFAULT NULL,
  forecast_condition VARCHAR(100) DEFAULT NULL,
  FOREIGN KEY (`control_id`)
REFERENCES forecast_weather_samples(`sample_id`)
);

CREATE TABLE forecast_summary (
  id INT AUTO_INCREMENT PRIMARY KEY,
  date TIMESTAMP NOT NULL,
  city VARCHAR(40) NOT NULL,
  provider VARCHAR(40) NOT NULL,
  forecast_range INT DEFAULT 1,
  on_interval_of_days INT DEFAULT 1,
  condition_correlation_rsd DECIMAL(4,1) DEFAULT 0,
  temperature_rsd DECIMAL(4,1) DEFAULT 0
);

CREATE TABLE forecast_summary_buffer (
  id INT AUTO_INCREMENT PRIMARY KEY,
  date TIMESTAMP NOT NULL,
  city VARCHAR(40) NOT NULL,
  provider VARCHAR(40) NOT NULL,
  forecast_range INT DEFAULT 1,
  on_interval_of_days INT DEFAULT 1,
  condition_correlation_rsd DECIMAL(4,1) DEFAULT 0,
  temperature_rsd DECIMAL(4,1) DEFAULT 0
);

CREATE TABLE proposals (
  id INT AUTO_INCREMENT PRIMARY KEY,
  date_time TIMESTAMP NOT NULL,
  type VARCHAR(40) NOT NULL,
  name VARCHAR(40) NOT NULL,
  email VARCHAR(40) NOT NULL,
  subject VARCHAR(40) NOT NULL,
  details VARCHAR(1500) NOT NULL
);

CREATE TABLE `person` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
   city VARCHAR(40),
   fav_provider VARCHAR(40),
  `password` varchar(200) NOT NULL,
  `role_id` int NOT NULL,
   PRIMARY KEY (`id`),
   FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

CREATE TABLE `condition_matrix` (
  severity_level int NOT NULL AUTO_INCREMENT,
  condition_name varchar(100) NOT NULL,
  sunny DECIMAL(3,2) NOT NULL,
  partly_cloudy DECIMAL(3,2) NOT NULL,
  cloudy_with_clearings DECIMAL(3,2) NOT NULL,
  cloudy DECIMAL(3,2) NOT NULL,
  rain DECIMAL(3,2) NOT NULL,
  thunder DECIMAL(3,2) NOT NULL,
  PRIMARY KEY (`severity_level`)
);

CREATE TABLE `sinoptik_condition_names` (
  severity_level int NOT NULL AUTO_INCREMENT,
  condition_name varchar(100) NOT NULL,
  FOREIGN KEY (severity_level) REFERENCES correlation_matrix(severity_level)
);

CREATE TABLE `gismeteo_condition_names` (
  severity_level int NOT NULL AUTO_INCREMENT,
  condition_name varchar(100) NOT NULL,
  FOREIGN KEY (severity_level) REFERENCES correlation_matrix(severity_level)
);


CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
   PRIMARY KEY (`role_id`)
);